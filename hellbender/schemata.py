from axolotl.schema import experimenters, subjects, equipment, recordings, protocols, rnaseq, photoreceptors, testing, misc, imaging, ephys

CAN_ADD_COLUMNS = ['rnaseq', 'testing', 'misc']
SCHEMA = {
        'experimenters':experimenters,
        'subjects':subjects,
        'equipment':equipment,
        'recordings':recordings,
        'protocols':protocols,
        'rnaseq':rnaseq,
        'photoreceptors':photoreceptors,
        'testing':testing,
        'misc':misc,
        'imaging':imaging,
        'ephys':ephys
        }

for schema_name, schema in SCHEMA.items():
    getattr(schema, 'schema_'+schema_name).spawn_missing_classes()

