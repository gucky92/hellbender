import numpy as np
import pandas as pd

from json import dumps as json_dumps

import random
import string

import csv

from ..schemata import protocols
from scipy.interpolate import interp1d

columns = [
	'led',
	'duration',
	'tstart',
	'stim_type',
	'intensity_type',
	'intensity_ref',
	'intensity',
	'random_type',
	'uniform_min',
	'uniform_max',
	'gaussian_mean',
	'gaussian_variance',
	'offset',
	'sine_freq',
	'sine_phase',
	'mix'
]

def id_generator(size=6, chars=string.ascii_uppercase+string.digits):
    """Generate a random ID with a given size
    """
    return ''.join(random.choice(chars) for _ in range(size))

def get_form_param(form, param, idx=None):
    if idx == None:
        return form.get(param)
    else:
    	if param in ['led', 'type']:
    		return form.get('led_fields-' + str(idx) + '-' + param)
    	elif param in ['timestamps', 'durations', 'intensities']:
    		return [float(x) for x in form.get('led_fields-' + str(idx) + '-' + param)[1:-1].split(',')]
    	elif param == 'refs':
    		return [int(x) for x in form.get('led_fields-' + str(idx) + '-' + param)[1:-1].split(',')]
    	elif param == 'types':
    		return [s[1:-1] for s in form.get('led_fields-' + str(idx) + '-' + param)[1:-1].split(',')]
    	elif param == 'intensity':
    		return form.get('led_fields-' + str(idx) + '-' + param).split(';')
    	elif param == 'in_mix':
    		return form.get('led_fields-' + str(idx) + '-' + param) == 'on'
    	else:
        	return float(form.get('led_fields-' + str(idx) + '-' + param))

def get_param(data, param, idx=None):
    if idx == None:
        return data[param]
    else:
    	led_data_param = data['led_fields-' + str(idx)][param]
    	if param in ['led', 'type']:
    		return led_data_param
    	elif param in ['timestamps', 'durations', 'intensities']:
    		return [float(x) for x in led_data_param[1:-1].split(',')]
    	elif param == 'refs':
    		return [int(x) for x in led_data_param[1:-1].split(',')]
    	elif param == 'types':
    		return [s[1:-1] for s in led_data_param[1:-1].split(',')]
    	elif param == 'intensity':
    		return led_data_param.split(';')
    	elif param == 'in_mix':
    		return led_data_param == 'on'
    	else:
        	return float(led_data_param)

def shift_event(led, duration, tstart):
	return (
		led,
		duration,
		tstart,
		'step',
		'shift',
		-1,
		'0.0',
		None,
		np.nan,
		np.nan,
		np.nan,
		np.nan,
		0.,
		np.nan,
		np.nan,
		-1.
		)

def rest_event(led, duration, tstart, offset):
	return (
		led,
		duration,
		tstart,
		'step',
		'rest',
		-1,
		'0.0',
		None,
		np.nan,
		np.nan,
		np.nan,
		np.nan,
		offset,
		np.nan,
		np.nan,
		-1.
		)

def constant_step_event(led, duration, tstart, offset, intensity, mix):
	return (
		led,
		duration,
		tstart,
		'step',
		'constant',
		-1,
		intensity,
		None,
		np.nan,
		np.nan,
		np.nan,
		np.nan,
		offset,
		np.nan,
		np.nan,
		mix
		)

def uniform_step_event(led, duration, tstart, offset, min_val, max_val, mix):
	return (
		led, 
		duration, 
		tstart, 
		'step', 
		'random', 
		-1, 
		None, 
		'uniform', 
		min_val, 
		max_val, 
		np.nan, 
		np.nan, 
		offset, 
		np.nan, 
		np.nan,
		mix
		)

def gaussian_step_event(led, duration, tstart, mean, variance):
	return (
		led, 
		duration, 
		tstart, 
		'step', 
		'random', 
		-1, 
		None, 
		'gaussian', 
		np.nan, 
		np.nan, 
		mean, 
		variance, 
		mean, 
		np.nan, 
		np.nan, 
		-1
		)

def ref_step_event(led, duration, tstart, ref_idx):
	return (
		led, 
		duration, 
		tstart, 
		'step', 
		'ref', 
		ref_idx, 
		None, 
		None, 
		np.nan, 
		np.nan, 
		np.nan, 
		np.nan, 
		0., 
		np.nan, 
		np.nan,
		-1
		)

def sine_event(led, duration, tstart, offset, intensity, freq, phase):
	return (
		led, 
		duration, 
		tstart, 
		'sine', 
		'constant', 
		-1, 
		None, 
		None, 
		np.nan, 
		np.nan, 
		np.nan, 
		np.nan, 
		offset, 
		freq, 
		phase, 
		-1
		)

def init_events():
	return [[0]]

def set_events_format(events):
	events = pd.DataFrame(np.array(events, ndmin=2), columns=columns).infer_objects().sort_values('tstart').to_records(False)
	# dtypes = [
	# 	('led', 'U'),
	# 	('duration', '>f8'),
	# 	('tstart', '>f8'),
	# 	('stim_type', 'U'),
	# 	('intensity_type', 'U'),
	# 	('intensity_ref', 'u8'),
	# 	('intensity', '<f8'),
	# 	('random_type', 'U'),
	# 	('uniform_min', '>f8'),
	# 	('uniform_max', '>f8'),
	# 	('gaussian_mean', '>f8'),
	# 	('gaussian_variance', '>f8'),
	# 	('offset', '>f8'),
	# 	('sine_freq', '>f8'),
	# 	('sine_phase', '>f8'),
	# ]
	return events

def add_event(events, event):
	if events[0][0] == 0:
		events[0] = event
	else:
		events.append(event)

def create_events(data, idx=0):
	events = init_events()

	led = get_param(data, 'led', idx)
	wave = get_param(data, 'type', idx)

	timestamps = get_param(data, 'timestamps', idx)
	durations = get_param(data, 'durations', idx)
	event_intensities = get_param(data, 'intensities', idx)
	refs = get_param(data, 'refs', idx)
	types = get_param(data, 'types', idx)

	intensities = get_param(data, 'intensity', idx)
	offset = get_param(data, 'offset', idx)
	mean = get_param(data, 'mean', idx)
	variance = get_param(data, 'variance', idx)
	freq = get_param(data, 'freq', idx)
	phase = get_param(data, 'phase', idx)

	mixing_count = get_param(data, 'mix', 0)
	in_mix = get_param(data, 'in_mix', idx)
	mix = mixing_count if (mixing_count != 0 and (idx == 0 or in_mix)) else -1

	gaussian_intensities = []

	for k in range(len(timestamps)):
		dur = durations[k]
		tstart = timestamps[k]
		intensity = str(event_intensities[k])
		ref = refs[k]
		stim_type = types[k]

		if stim_type == 'shift':
			add_event(events, shift_event(led, dur, tstart))

		elif stim_type == 'rest':
			if wave in ['steps', 'sinew']:
				add_event(events, rest_event(led, dur, tstart, offset))
			elif wave == 'gnoise':
				add_event(events, rest_event(led, dur, tstart, mean))

		elif stim_type == 'constant':
			add_event(events, constant_step_event(led, dur, tstart, offset, ','.join(intensities), mix if (offset != 0. or intensity != '0.0') else -1.))

		elif stim_type == 'random':
			if ref >= 0:
				add_event(events, ref_step_event(led, dur, tstart, ref))
			elif ref == -1:
				if len(intensities) == 1:
					add_event(events, uniform_step_event(led, dur, tstart, offset, 0, intensities[0], mix))
				else:
					add_event(events, constant_step_event(led, dur, tstart, offset, 'shuffle-' + ','.join(intensities), mix))
					# Tell csv to pick randomly among the intensity values and do all of them

		elif stim_type == 'gaussian':
			if ref >= 0:
				add_event(events, ref_step_event(led, dur, tstart, ref))
			elif ref == -1:
				add_event(events, gaussian_step_event(led, dur, tstart, mean, variance))
			elif ref == -2:
				add_event(events, constant_step_event(led, dur, tstart, offset, 'shuffle-gaussian', -1))
				# Handle shuffle on gaussian values from first series of events

		elif stim_type == 'sine':
			add_event(events, sine_event(led, dur, tstart, offset, intensity, freq, phase))

		elif k == 0 and stim_type == 'shift':
			for j in range(0, len(timestamps)):
				if refs[j] >= 0:
					refs[j] -= 1

	return events

def create_all_events(data):
	events = create_events(data, 0)
	k = 1
	while ('led_fields-' + str(k)) in data:
		events = events + create_events(data, k)
		k += 1

	return set_events_format(events)

def format_form_data(form):
	data = {
		'form_data': "true"
	}

	for field in form:
		if 'led_fields' in field:
			field_info = field.split('-')
			led_fields = '-'.join(field_info[0:2])
			param = field_info[2]
			if led_fields not in data:
				data[led_fields] = {}
			data[led_fields][param] = form.get(field)
	data['ledsystem_name'] = form.get('ledsystem_name')
	return data

def generate_csv_files(stimulus):
	events = stimulus['exp_array']
	ledsystem_name = stimulus['exp_arr_format']['ledsystem_name']
	filename = stimulus['exp_filepath'] + '/settings_' + stimulus['date_exp_created'].strftime("%Y-%m-%d") + '__' + stimulus['protocol_name']

	data = {}
	intensity_idx = {}
	gaussian_intensities = []
	gaussian_intensity_idx = {}
	for event in events:
		led = event['led']
		if led not in data:
			data[led] = []
			intensity_idx[led] = 0
			gaussian_intensity_idx[led] = 0

		dur = event['duration']
		offset = event['offset']
		intensity = event['intensity']
		intensities = intensity.split('-')
		shuffle = (len(intensities) == 2)
		intensities = intensities[len(intensities) - 1].split(',')

		if event['stim_type'] == 'step':
			if event['intensity_type'] == 'ref':
				value = data[led][event['intensity_ref']]

			elif event['intensity_type'] == 'shift':
				value = 0.

			elif event['intensity_type'] == 'rest':
				value = offset

			elif event['intensity_type'] == 'constant':
				if intensities[0] == 'gaussian':
					if shuffle and gaussian_intensity_idx[led] == 0:
						gaussian_intensities = random.shuffle(gaussian_intensities)

					value = gaussian_intensities[gaussian_intensity_idx[led]]
					gaussian_intensity_idx[led] = (gaussian_intensity_idx[led] + 1) % len(gaussian_intensities)

				elif len(intensities) == 1:
					value = offset + float(intensity)

				else:
					if shuffle and intensity_idx[led] == 0:
						intensities = random.shuffle(intensities)

					value = offset + float(intensities[intensity_idx[led]])
					intensity_idx[led] = (intensity_idx[led] + 1) % len(intensities)

			elif event['intensity_type'] == 'random':
				if event['random_type'] == 'uniform':
					value = offset + random.randint(event['uniform_min'], event['uniform_max'])

				elif event['random_type'] == 'gaussian':
					value = offset + random.gauss(event['gaussian_mean'], event['gaussian_variance'])
					gaussian_intensities.append(value)

			data[led].append([dur, value])

		elif event['stim_type'] == 'sine':
			for i in range(0, dur, 2):
				value = (offset + float(intensity)) * np.sin(2 * np.pi * (event['sine_freq'] * i / 1000. + event['sine_phase'] / 360.))
				data[led].append([2., value])

	fieldnames = ['duration', 'intensity']
	for led in data:
		led_calibs = (protocols.LedMeasurement.proj('led_name', 'ledsystem_name', 'date_measured') * protocols.LedMeasurementImported.proj('led_volts', 'led_watts_per_mm2') & {'ledsystem_name': ledsystem_name, 'led_name': led}).fetch()
		led_calibs.sort(order='date_measured')
		calib_func = interp1d(led_calibs[-1]['led_watts_per_mm2'], led_calibs[-1]['led_volts'], bounds_error=False, fill_value=0.)

		with open(filename + '_' + led + '.csv', 'w') as csvfile:
			writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

			for event in data[led]:
				if event[1] != 0.:
					event[1] = calib_func(event[1])
				writer.writerow(event)