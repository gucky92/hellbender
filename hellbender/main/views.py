import pandas as pd
from flask import render_template, redirect, url_for, flash, request, session, send_from_directory
import datajoint as dj
import uuid
import graphviz
import numpy as np
import flask
import datetime
from datajoint.utils import to_camel_case

from flask_login import login_user, login_required
from . import main, forms
from werkzeug.urls import url_parse
from werkzeug.utils import secure_filename
from .. import User
from .. import schemata
from ..schemata import subjects, protocols, experimenters, recordings, equipment
from .. import form_factory
from ..datatable import json_table
from .. import upload_factory
from .. import ALLOWED_EXTENSIONS, UPLOAD_FOLDER
from ..errors import HellBenderError
import axolotl

from . import stimulus_helper
import json

import os
from importlib import reload

#
ENTRY_LIMIT = 10
DEFAULT_TABLE = {'schema':'experimenters', 'table':'Experimenter', 'subtable':None}
#
def ping(f):
    """ Decorator to keep database connection alive."""

    def wrapper(*args, **kwargs):
        try:
            dj.conn().ping()
        except:
            try:
                flash('attempting to connect to database', 'error')
                reload(dj)
                reload(axolotl)
                axolotl.conn()
                dj.conn()
            except Exception as e:
                flash('cannot connect to database: ' + str(e), 'error')
        return f(*args, **kwargs)

    return wrapper


def escape_json(json_string):
    """ Clean JSON strings so they can be used as html attributes."""
    return json_string.replace('"', '&quot;')

@ping
@main.route('/', methods=["GET"])
@main.route('/index', methods=["GET"])
@login_required
def index():
    return render_template('index.html')

@ping
@main.route('/login', methods=['GET', 'POST'])
def login():
    session['schemata'] = list(schemata.SCHEMA.keys())
    form = forms.UserForm(request.form)
    if request.method == 'POST' and form.validate():
        u = User(form.user.data)
        if u.check_user() is None or not u.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('main.login'))
        login_user(u)
        session['user'] = form.user.data
        #
        try:
            reload(axolotl)
            dj.conn()
        except Exception as e:
            flash('cannot connect to database: ' + str(e), 'error')
        #
        flask.flash('Logged in successfully.')
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('main.index')
        return redirect(next_page)
    return flask.render_template('user.html', form=form)

@ping
@main.route('/register', methods=['GET', 'POST'])
def register():
    form = forms.RegisterForm(request.form)
    if request.method == 'POST' and form.validate():
        u = User(form.user.data)
        if u.check_user() is None or not u.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('main.register'))
        u.set_password(form.new_password.data)
        login_user(u)
        session['user'] = form.user.data
        axolotl.conn(session['user'], 'dr0s0phila')
        flask.flash('Logged in successfully.')
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('main.index')
        return redirect(next_page)
    return flask.render_template('register.html', form=form)


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@ping
@main.route('/upload', methods=['GET', 'POST'])
@login_required
def upload():
    form = forms.SchemaForm(request.form)
    if request.method == 'POST' and form.validate():
        if 'file' not in request.files:
            flash('no file part')
            return redirect(request.url)
        f = request.files['file']
        #
        #
        if f.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if f and allowed_file(f.filename):
            filename = secure_filename(f.filename)
            flash('uploaded data')
            try:
                f.save(os.path.join(UPLOAD_FOLDER, filename))
            except PermissionError:
                try:
                    os.remove(os.path.join(UPLOAD_FOLDER, filename))
                    f.save(os.path.join(UPLOAD_FOLDER, filename))
                except:
                    flash('trying to overwrite protected file - change your filename', 'error')
            return redirect(
                    url_for('main.uploaded',
                        filename=filename,
                        schema=form.schema.data,
                        table=form.table.data
                        ))
    return flask.render_template('upload.html', form=form)

@ping
@main.route('/uploaded/<schema>/<table>' , methods=['GET', 'POST'])
@login_required
def uploaded(schema, table):
    filename = request.args.get('filename', None)
    if filename is None:
        return redirect(url_for('main.upload'))
    #
    rel = getattr(getattr(schemata, schema), table)
    data = pd.read_csv(os.path.join(UPLOAD_FOLDER, filename))
    form = upload_factory.check_form(rel, data)(request.form)
    if request.method == 'POST' and form.validate():
        try:
            form.insert()
        except HellBenderError as e:
            flash(str(e), 'error')
        flash('data inserted')
        #os.remove(os.path.join(UPLOAD_FOLDER, filename))
    return flask.render_template('uploaded.html', form=form)

@ping
@main.route('/tmp/<path:filename>')
@login_required
def tmpfile(filename):
    return send_from_directory(UPLOAD_FOLDER, filename)

@ping
@main.route('/temp/<path:filename>')
@login_required
def tempfile(filename):
    return send_from_directory(UPLOAD_FOLDER, filename, as_attachment=True)

@ping
@main.route('/join/', methods=['GET', 'POST'])
@login_required
def join():
    form = forms.JoinForm(request.form)
    if request.method == 'POST':
        if 'submit' in request.form:
            if request.form['submit'] == 'Add entry':
                form.table.append_entry()
            elif request.form['submit'] == 'Remove entry':
                form.table.pop_entry()
            elif request.form['submit'] == 'Join' and form.validate():
                data = [[t.data['schema'], t.data['table']] for idx, t in enumerate(form.table)]
                data = [np.concatenate(data).tolist()]
                return redirect(url_for('main.joining', data=data))

    return render_template('join.html', form=form)

@ping
@main.route('/heading/', methods=['GET', 'POST'])
@login_required
def heading():
    form = forms.HeadingForm(request.form)
    #if not request.host in CREATE_HOST_URL:
    #    return redirect(CREATE_HOST_URL)
    if request.method == 'POST':
        if 'submit' in request.form:
            if request.form['submit'] == 'Add Attribute':
                form.attribute.append_entry()
            elif request.form['submit'] == 'Remove Attribute':
                form.attribute.pop_entry()
            elif request.form['submit'] == 'Submit' and form.validate():
                try:
                    form.insert_table_class()
                    flash('added table')
                except HellBenderError as e:
                    flash(str(e), 'error')
    #
    return render_template('heading.html', form=form)

@ping
@main.route('/addcolumn/', methods=['GET', 'POST'])
@login_required
def addcolumn():
    form = forms.AddColumnForm(request.form)
    #if not request.host in CREATE_HOST_URL:
    #    return redirect(CREATE_HOST_URL)
    if request.method == 'POST':
        if 'submit' in request.form:
            if request.form['submit'] == 'Add Attribute':
                form.attribute.append_entry()
            elif request.form['submit'] == 'Remove Attribute':
                form.attribute.pop_entry()
            elif request.form['submit'] == 'Submit' and form.validate():
                try:
                    form.add_columns()
                    flash('added columns')
                except HellBenderError as e:
                    flash(str(e), 'error')
    #
    return render_template('addcolumns.html', form=form)

@ping
@main.route('/joining/', methods=['GET', 'POST'])
@login_required
def joining():
    #
    data = eval(request.args.get('data'))
    print(data)
    print(type(data))
    tables = []
    for schema, table_name in zip(data[::2], data[1::2]):
        try:
            table, subtable = table_name.split('.')
        except:
            table = table_name
            subtable = None
        table = getattr(getattr(schemata, schema), table)
        if subtable is not None:
            table = getattr(table, subtable)
        tables.append(table)

    rel = dj.superjoin(tables)

    return render_template(
            'joining.html',
            table_json=json_table(
                rel,
                projected=True,
                ),
            )

@ping
@main.route('/delete/', defaults=DEFAULT_TABLE,
            methods=['GET', 'POST'])
@main.route('/delete/<schema>/<table>', defaults={'subtable':None}, methods=['GET', 'POST'])
@main.route('/delete/<schema>/<table>/<subtable>', methods=['GET', 'POST'])
@login_required
def delete(schema, table, subtable):
    #
    rel = getattr(getattr(schemata, schema), table)
    if subtable is not None:
        rel = getattr(rel, subtable)
    rel = rel()
    #
    if 'html_key' in request.args:
        restriction = request.args.get('html_key').replace('----', '#')
        restriction = pd.read_html("<table>" + restriction + "</table>")[0].infer_objects()
        restriction = restriction[list(set(rel.heading.primary_key) & set(restriction.columns))].iloc[0].to_dict()
        print(restriction)
    else:
        restriction = {k:v for k, v in request.args.items()}
    #
    entry = rel & restriction
    if not len(entry) == 1:
        raise HellBenderError("Delete only works on one entry at a time")
    #
    attributes = entry.proj(*entry.heading.non_blobs).fetch1()
    #
    deps = [dep for key, dep in rel.dependents().items()]
    dependents = []
    for dep in deps:
        if set(entry.heading.primary_key) & set(dep.heading):
            if dep & entry.proj():
                dependents.append(dep.full_table_name)
    #
    redirect_url = request.args.get('target', None)
    #
    if request.method == 'POST':
        if 'delete' in request.form:
            if request.form['delete'] == 'Delete':
                try:
                    dj.config['safemode'] = False
                    try:
                        entry.delete_quick()
                    except:
                        entry.delete(verbose=False)
                    flash('Deleted entry and dependent entries')
                    dj.config['safemode'] = True
                except dj.DataJointError:
                    dj.config['safemode'] = True
                    flash('Could not delete entry. Cascade delete error!', 'error')
                if redirect_url is not None:
                    return redirect(redirect_url)
                else:
                    return redirect(url_for('main.relation', schema=schema, table=table))
        if 'cancel' in request.form:
            if request.form['cancel'] == 'Cancel':
                if redirect_url is not None:
                    return redirect(redirect_url)
                else:
                    return redirect(url_for('main.relation', schema=schema, table=table))

    return render_template('delete.html', attributes=attributes, dependents=dependents)

def rm_hidden_entries(form):
    hidden_entries =[]
    for multi_part in form._multi_parts:
        hidden_entry = getattr(form, multi_part).entries.pop(0)
        hidden_entries.append(hidden_entry)
    return hidden_entries

def append_hidden_entries(form, hidden_entries):
    for multi_part, hidden_entry in zip(form._multi_parts, hidden_entries):
        getattr(form, multi_part).entries.insert(0, hidden_entry)

@ping
@main.route('/enter/', defaults=DEFAULT_TABLE,
            methods=['GET', 'POST'])
@main.route('/enter/<schema>/<table>', defaults={'subtable':None}, methods=['GET', 'POST'])
@main.route('/enter/<schema>/<table>/<subtable>', methods=['GET', 'POST'])
@login_required
def enter(schema, table, subtable):
    #
    rel_name = schema+table
    rel = getattr(getattr(schemata, schema), table)
    if subtable is not None:
        rel = getattr(rel, subtable)
        rel_name += subtable
    #
    redirect_url = request.args.get('target', None)
    enter_form = form_factory.check_form(rel, rel_name, skip_parts=False, multi_parts=True)(
            experimenter_name=session['user'])#(request.form, )
    if request.method == 'POST':
        hidden_entries = rm_hidden_entries(enter_form)
        if request.form['submit'] == 'Submit':
            if enter_form.validate_on_submit():
                enter_form.insert()
                flash("Data has been entered")
                append_hidden_entries(enter_form, hidden_entries)
                if redirect_url is None:
                    return redirect(url_for('main.relation', schema=schema, table=table, subtable=subtable))
                else:
                    return redirect(redirect_url)
            else:
                flash("Data could not be entered. Look at errors", 'error')
                append_hidden_entries(enter_form, hidden_entries)
                return render_template('datajoint_form.html', form=enter_form)
        elif request.form['submit'] == 'SubmitStay':
            if enter_form.validate_on_submit():
                enter_form.insert()
                flash("Data has been entered")
            else:
                flash("Data could not be entered. Look at errors", 'error')
            append_hidden_entries(enter_form, hidden_entries)
            return render_template('datajoint_form.html', form=enter_form)
        else:
            append_hidden_entries(enter_form, hidden_entries)
        if redirect_url is None:
            return redirect(url_for('main.relation', schema=schema, table=table, subtable=subtable))
        else:
            return redirect(redirect_url)
    return render_template('datajoint_form.html', form=enter_form)

@ping
@main.route('/edit/', defaults=DEFAULT_TABLE,
            methods=['GET', 'POST'])
@main.route('/edit/<schema>/<table>', defaults={'subtable':None}, methods=['GET', 'POST'])
@main.route('/edit/<schema>/<table>/<subtable>', methods=['GET', 'POST'])
@login_required
def edit(schema, table, subtable):
    """
    """
    rel_name = schema+table
    rel = getattr(getattr(schemata, schema), table)
    if subtable is not None:
        rel = getattr(rel, subtable)
        rel_name += subtable
    #
    redirect_url = request.args.get('target', None)
    if 'html_key' in request.args:
        kwargs = request.args.get('html_key').replace('----', '#')
        kwargs = pd.read_html("<table>" + kwargs + "</table>")[0].infer_objects()
        kwargs = kwargs[list(set(rel.heading) & set(kwargs.columns))].iloc[0]
        kwargs = kwargs[(kwargs != 'None') & (kwargs != '<BLOB>') & (kwargs != '')].to_dict()
    else:
        kwargs = {k:v for k, v in request.args.items()}
    #
    form_name = request.args.get('form_name', None)
    #
    form_names = [key for key in form_factory.store if key.startswith(str(form_name))]
    #
    if form_name is None:
        edit_form = form_factory.check_form(rel, rel_name, skip_parts=True)(**kwargs)
    elif form_name in form_factory.store:
        edit_form = form_factory.store[form_name](**kwargs)
    elif len(form_names) == 1:
        edit_form = form_factory.store[form_names[0]](**kwargs)
    else:
        raise HellBenderError(f'form {form_name} not in factory')
    #
    #
    if request.method == 'POST':
        if 'submit' in request.form:
            if request.form['submit'] == 'Replace' and edit_form.validate_on_submit():
                edit_form.insert(replace=True)
                flash("Data has been replaced")
                if redirect_url is None:
                    return redirect(url_for('main.relation', schema=schema, table=table))
                else:
                    return redirect(redirect_url)
            if request.form['submit'] == 'Update' and edit_form.validate_on_submit():
                edit_form.insert(update=True)
                flash("Data has been updated")
                if redirect_url is None:
                    return redirect(url_for('main.relation', schema=schema, table=table))
                else:
                    return redirect(redirect_url)
            if request.form['submit'] == 'Cancel':
                if redirect_url is None:
                    return redirect(url_for('main.relation', schema=schema, table=table))
                else:
                    return redirect(redirect_url)
    #
    return render_template('datajoint_edit.html', form=edit_form)


@ping
@main.route('/subjectentry', methods=['GET', 'POST'])
@login_required
def subjectentry():
    rel = subjects.FlySubject
    #
    kwargs = {}
    #
    if 'genotype_id' in request.args:
        kwargs['genotype_id'] = request.args.get('genotype_id')
    if not 'experimenter_name' in kwargs:
        kwargs['experimenter_name'] = session['user']
    if not 'subject_name' in kwargs:
        initials = (experimenters.Experimenter & {'experimenter_name':session['user']}).fetch1()['experimenter_initials']
        date = datetime.datetime.now().strftime("%y%m%d")
        prefix = initials + date + '#'
        prev = rel.proj().search(prefix).fetch()
        if len(prev) > 0:
            prev = pd.DataFrame(prev)['subject_name'].str.replace(prefix, '').astype(int).max()
            kwargs['subject_name'] = prefix + str(prev+1).zfill(3)
        else:
            kwargs['subject_name'] = prefix + '000'
    enter_form = form_factory.check_form(rel, 'subjectentry')(request.form, **kwargs)
    #
    if request.method == 'POST':
        if request.form['submit'] == 'Submit':
            if enter_form.validate():
                enter_form.insert()
                flash("Subject data has been entered")
                return redirect(url_for('main.selectsystem', subject_name=enter_form.subject_name.data))
            flash("Data could not be entered. Look at errors", 'error')
    ###
    return render_template(
            'subjectentry.html',
            table_json=json_table(
                subjects.FlyGenotype,
                projected=True,
                edit_url=url_for('main.edit', schema='subjects', table='FlyGenotype'),
                ),
            form=enter_form
            )

@ping
@main.route('/selectsystem', methods=['GET', 'POST'])
@login_required
def selectsystem():
    subject_name = request.args.get('subject_name', None)
    if subject_name is None:
        return redirect(url_for('main.subjectentry'))
    if request.method == 'POST':
        if request.form['submit'] == 'TwoPhoton':
            return redirect(url_for('main.recentry', subject_name=subject_name, part_table_name='TwoPhoton'))
        elif request.form['submit'] == 'Intracellular':
            return redirect(url_for('main.recentry', subject_name=subject_name, part_table_name='Intracellular'))
        elif request.form['submit'] == 'Misc':
            return redirect(url_for('main.recentry', subject_name=subject_name, part_table_name='Misc'))
    return render_template('selectsystem.html', subject_name=subject_name)

@ping
@main.route('/flysubjects/', methods=['GET', 'POST'])
@login_required
def flysubjects():
    #
    rel = dj.superjoin([subjects.FlySubject, subjects.FlySubject.Prep])
    #
    if request.method == 'POST':
        if request.form['submit'] == 'Start Experiment':
            return redirect(url_for('main.subjectentry'))
    target = url_for('main.flysubjects')
    return render_template(
            'flysubjects.html',
            table_json=json_table(
                rel,
                projected=True,
                delete_url=url_for('main.delete', schema='subjects', table='FlySubject'),
                recording_url=url_for('main.recentry'),
                edit_url=url_for('main.edit', schema='subjects', table='FlySubject'),
                ),
            target=target,
            )

@ping
@main.route('/recentry', methods=['GET', 'POST'])
@login_required
def recentry():
    rel = recordings.Recording
    subject_name = request.args.get('subject_name', None)
    part_table_name = request.args.get('part_table_name', None)
    if 'html_key' in request.args:
        kwargs = request.args.get('html_key').replace('----', '#')
        kwargs = pd.read_html("<table>" + kwargs + "</table>")[0].infer_objects()
        subject_name = kwargs[list(set(rel.heading) & set(kwargs.columns))].iloc[0]['subject_name']
    if part_table_name is None and subject_name is None:
        return redirect(url_for('main.selectsystem', subject_name=subject_name))
    elif part_table_name is None:
        if len((rel & {'subject_name':subject_name}).fetch()) == 0:
            return redirect(url_for('main.selectsystem', subject_name=subject_name))
        #check which part_table contains the subject name
        for part_table in rel().part_tables():
            #TODO if more than one part table is filled
            if len((part_table & {'subject_name':subject_name}).fetch()) > 0:
                part_table_name = to_camel_case(part_table.full_table_name.split('__')[-1].replace('`',''))
                break
        else:
            part_table_name = 'Misc'
    ###
    if part_table_name == 'Misc':
        skip_parts = False
        select_parts = None
        multi_parts = True
    else:
        skip_parts = False
        part_table = getattr(rel, part_table_name)
        select_parts = [part_table.full_table_name]
        extra_primary_key = set(part_table.heading.primary_key) - set(rel.heading.primary_key)
        if len(extra_primary_key) == 0:
            multi_parts = False
        else:
            multi_parts = True
    #
    if part_table_name == 'TwoPhoton':
        datapath = '/mnt/engram/raw2p/2p_data'
    elif part_table_name == 'Intracellular':
        datapath = '/mnt/engram/rawEP'
    else:
        datapath = '/mnt/engram'
    #
    rel = rel & {'subject_name':subject_name}
    #
    existing_recs = rel.proj().fetch()
    if len(existing_recs) == 0:
        recording_id = 1
    else:
        recording_id = (existing_recs['recording_id']).max() + 1
    #
    #
    form_name = 'recentry_' + str(part_table_name)
    kwargs = {k:v for k, v in request.args.items() if not k in ['subject_name', 'recording_id']}
    #
    if 'recording_datapath' in kwargs:
        datapath = kwargs['recording_datapath'] + 1
        del(kwargs['recording_datapath'])
    #
    #
    #
    enter_form = form_factory.check_form(
            rel, form_name, select_parts=select_parts,
            skip_parts=skip_parts, multi_parts=multi_parts
            )(
            request.form,
            subject_name=subject_name, recording_id=recording_id,
            recording_datapath=datapath,
            **kwargs
            )
    #
    ###
    target = url_for('main.recentry',
            subject_name=subject_name,
            part_table_name=part_table_name,
            form=enter_form,
            )
    #
    if request.method == 'POST':
        if 'submit' in request.form:
            if request.form['submit'] == 'Submit':
                hidden_entries = rm_hidden_entries(enter_form)
                if enter_form.validate():
                    datapath = enter_form.recording_datapath.data
                    dataname = enter_form.recording_dataname.data
                    exists = (rel & {'recording_datapath':datapath, 'recording_dataname':dataname}).fetch()
                    exists = bool(len(exists)) & (part_table_name == 'TwoPhoton')
                    if not exists:
                        enter_form.insert()
                        flash("Recording data has been entered")
                        #return redirect(target)
                    else:
                        flash("Recording with this datapath and dataname already exists!")
                else:
                    flash("Data could not be entered. Look at errors", 'error')
                append_hidden_entries(enter_form, hidden_entries)
            if request.form['submit'] == 'Add Protocol':
                return redirect(
                        url_for('main.enter',
                            schema='protocols',
                            target=target, table='ExperimentalProtocol'))
    #
    #
    return render_template(
            'recentry.html',
            table_json2=json_table(
                dj.superjoin([
                    subjects.FlySubject,
                    subjects.FlySubject.Prep], restrictions={'subject_name':subject_name}),
                projected=True,
                table_name='subjecttable',
                edit_url=url_for('main.edit', schema='subjects', table='FlySubject'),
                ),
            table_json=json_table(
                rel,
                projected=True,
                delete_url=url_for('main.delete', schema='recordings', table='Recording'),
                edit_url=url_for('main.edit', schema='recordings', table='Recording'),
                ),
            form=enter_form,
            part_table_name=part_table_name,
            subject_name=subject_name
            )

@ping
@main.route('/enter_genotype/', methods=['GET', 'POST'])
@login_required
def enter_genotype():
    #entry of fly genotype
    rel = subjects.FlyGenotype
    ###
    kwargs= {}
    if not 'experimenter_name' in kwargs:
        kwargs['experimenter_name'] = session['user']
    enter_form = form_factory.check_form(rel, rel.full_table_name)(
            request.form,
            **kwargs
            )

    if request.method == 'POST':
        #
        if request.form['submit'] in ['Submit', 'Stock', 'PersonalStock', 'Cross', 'Experiment']:
            if enter_form.validate():
                enter_form.insert(skip_duplicates=True)
                flash("Fly data has been entered")
            else:
                flash("Data could not be entered. Look at errors", 'error')
            if request.form['submit'] == 'Stock':
                return redirect(url_for('main.flyentry', genotype_id=enter_form.genotype_id.data))
            elif request.form['submit'] == 'PersonalStock':
                return redirect(url_for('main.flyentry', personal='personal',
                    genotype_id=enter_form.genotype_id.data, form=enter_form))
            elif request.form['submit'] == 'Cross':
                return redirect(url_for('main.cross', genotype_id=enter_form.genotype_id.data))
            elif request.form['submit'] == 'Experiment':
                return redirect(url_for('main.subjectentry', genotype_id=enter_form.genotype_id.data))

    return render_template(
            'enter_genotype.html',
            table_json=json_table(
                rel,
                projected=True,
                edit_url=url_for('main.edit', schema='subjects', table='FlyGenotype'),
                ),
            form=enter_form
            )
@ping
@main.route('/flyentry', defaults={'personal':None}, methods=['GET', 'POST'])
@main.route('/flyentry/<personal>', methods=['GET', 'POST'])
@login_required
def flyentry(personal=None):
    if personal == 'None':
        personal = None
    #
    if personal is None or personal == 'stock':
        stock = subjects.FlyStock
        form_name = 'FlyStock'
    else:
        stock = subjects.PersonalFlyStock
        form_name = 'PersonalFlyStock'
    #
    kwargs= {}
    if 'genotype_id' in request.args:
        kwargs['genotype_id'] = request.args.get('genotype_id')
    if not 'experimenter_name' in kwargs:
        kwargs['experimenter_name'] = session['user']
    enter_form = form_factory.check_form(stock, form_name)(
            request.form,
            **kwargs
            )
    rel = dj.superjoin([subjects.FlyGenotype, stock])

    if personal is None or personal == 'stock':
        next_stock = pd.Series(
                stock.proj().fetch()['stock_name']).apply(lambda x: int(x.replace('RBF', ''))).max() + 1
        next_stock = 'RBF' + str(next_stock)
        enter_form.stock_name.process_data(next_stock)
    ###
    ###
    if request.method == 'POST':
        #
        if request.form['submit'] == 'Submit':
            if enter_form.validate():
                enter_form.insert(skip_duplicates=True)
                flash("Fly data has been entered")
            else:
                flash("Data could not be entered. Look at errors", 'error')
    #
    return render_template(
            'flyentry.html',
            personal=personal,
            table_json=json_table(
                rel,
                projected=True,
                delete_url=url_for('main.delete', schema='subjects', table=form_name),
                edit_url=url_for('main.edit', schema='subjects', table=form_name),
                ),
            form=enter_form
            )

@ping
@main.route('/cross/', methods=['GET', 'POST'])
@login_required
def cross():
    #
    form_name = 'crossentry'
    #
    kwargs = {}
    if 'genotype_id' in request.args:
        kwargs['desired'] = request.args.get('genotype_id')
    if not 'experimenter_name' in kwargs:
        kwargs['experimenter_name'] = session['user']
    enter_form = form_factory.check_form(subjects.Crosses, form_name)(
            request.form, **kwargs)
    ###
    ###
    rel = subjects.FlyGenotype.proj(
            *subjects.FlyGenotype.heading, desired='genotype_id'
            ) * subjects.Crosses.proj(
            *[
                attr for attr in subjects.Crosses.heading
                if attr not in subjects.FlyGenotype.heading.dependent_attributes
                ]
            )
    ###
    if request.method == 'POST':
        if request.form['submit'] == 'Submit':
            if enter_form.validate():
                enter_form.insert(skip_duplicates=True)
                flash("Fly data has been entered")
            else:
                flash("Data could not be entered. Look at errors", 'error')

    return render_template(
            'cross.html',
            table_json=json_table(
                rel,
                projected=True,
                delete_url=url_for('main.delete', schema='subjects', table='Crosses'),
                edit_url=url_for('main.edit', schema='subjects', table='Crosses'),
                ),
            form=enter_form
            )

@ping
@main.route('/stimuli/', methods=['GET', 'POST'])
@login_required
def stimuli():
    #TODO make specific stimulus form class and automatic insertion of data path and such
    #TODO psychopy files or pyStim files
    rel = protocols.ExperimentalProtocol
    kwargs= {}
    protocol_name = request.form.get('protocol_name')
    kwargs['protocol_name'] = protocol_name if protocol_name is not None else stimulus_helper.id_generator()
    enter_form = form_factory.check_form(rel, 'Stimulus')(
        request.form,
        **kwargs
        )

    if request.method == 'POST':
        if request.form['submit'] == 'Submit':

            if enter_form['protocol_group'].data['protocol_group'] == 'led_system':
                enter_form['comments'].data = str(stimulus_helper.format_form_data(request.form))
                # enter_form['exp_array'].data = stimulus_helper.create_all_events(request.form)
                if enter_form.validate():
                    enter_form.insert(skip_duplicates=True)
                    flash("Stimulus has been entered")
                else:
                    flash("Data could not be entered. Look at errors", 'error')

            elif enter_form['protocol_group'].data['protocol_group'] == 'projector_led':
                if 'psychopy_file' not in request.files:
                    flash('No psychopy file part')
                    return redirect(request.url)
                f = request.files['psychopy_file']
                #
                #
                if f.filename == '':
                    flash('No psychopy selected file')
                    return redirect(request.url)
                if f and allowed_file(f.filename):
                    #filename = secure_filename(f.filename)
                    raise NotImplementedError('file upload')
                    #try:
                    #    stimulus['exp_filepath'] = os.path.join(UPLOAD_FOLDER, filename)
                    #    f.save(stimulus['exp_filepath'])
                    #except PermissionError:
                    #    try:
                    #        stimulus['exp_filepath'] = os.path.join(UPLOAD_FOLDER, filename)
                    #        os.remove(stimulus['exp_filepath'])
                    #        f.save(stimulus['exp_filepath'])
                    #    except:
                    #        flash('trying to overwrite protected file - change your filename', 'error')

        elif request.form['submit'] == 'Generate CSV':
            try:
                protocol_name = stimulus_helper.get_form_param(request.form, 'protocol_name')
                #ledsystem_name = stimulus_helper.get_form_param(request.form, 'ledsystem_name')
                stimulus = (protocols.ExperimentalProtocol() & {'protocol_name': protocol_name}).fetch1()
                stimulus_helper.generate_csv_files(stimulus)

            except dj.DataJointError:
                flash('You need to create the stimulus first', 'error')

    try:
        form_data = json.loads((protocols.ExperimentalProtocol() & {'protocol_name': enter_form['protocol_name'].data}).fetch1()['comments'])
        form_data.pop('timestamps', None)
        form_data.pop('durations', None)
        form_data.pop('intensities', None)
        form_data.pop('types', None)
        form_data.pop('refs', None)
        form_data = str(form_data)
    except dj.DataJointError:
        form_data = ''

    leds = {}
    for led in equipment.LedSystem().fetch():
        if not led['ledsystem_name'] in leds:
            leds[led['ledsystem_name']] = []
        leds[led['ledsystem_name']].append(led['led_name'])

    return render_template(
        'stimuli.html',
        table_json=json_table(
            rel,
            projected=True,
            delete_url=url_for('main.delete', schema='protocols', table='ExperimentalProtocol'),
            edit_url=url_for('main.edit', schema='protocols', table='ExperimentalProtocol'),
            ),
        form=enter_form,
        form_data=form_data,
        leds=leds
        )

@ping
@main.route('/schema/', defaults=DEFAULT_TABLE,
            methods=['GET', 'POST'])
@main.route('/schema/<schema>/<table>', defaults={'subtable': None}, methods=['GET', 'POST'])
@main.route('/schema/<schema>/<table>/<subtable>', methods=['GET', 'POST'])
@login_required
def relation(schema, table, subtable):
    #draw ERD
    graph_attr = {'size': '12, 12', 'rankdir': 'LR', 'splines': 'ortho'}
    node_attr = {'style': 'filled', 'shape': 'note', 'align': 'left', 'ranksep': '0.1',
                 'fontsize': '10', 'fontfamily': 'opensans', 'height': '0.2',
                 'fontname': 'Sans-Serif'}
    dot = graphviz.Digraph(graph_attr=graph_attr, node_attr=node_attr, engine='dot',
                           format='svg')

    def add_node(name, node_attr={}):
        """ Add a node/table to the current graph (adding subgraphs if needed). """
        table_names = dict(zip(['schema', 'table', 'subtable'], name.split('.')))
        graph_attr = {'color': 'grey80', 'style': 'filled', 'label': table_names['schema']}
        with dot.subgraph(name='cluster_{}'.format(table_names['schema']), node_attr=node_attr,
                          graph_attr=graph_attr) as subgraph:
            subgraph.node(name, label=name, URL=url_for('main.relation', **table_names),
                          target='_top', **node_attr)
        return name

    def name_lookup(full_name):
        """ Look for a table's class name given its full name. """
        pretty_name = dj.base_relation.lookup_class_name(full_name, schemata.__dict__)
        return pretty_name or full_name

    root_rel = getattr(getattr(schemata, schema), table)
    root_rel = root_rel if subtable is None else getattr(root_rel, subtable)
    root_dependencies = root_rel.connection.dependencies
    root_dependencies.load()

    node_attrs = {dj.Manual: {'fillcolor': 'green3'}, dj.Computed: {'fillcolor': 'coral1'},
                  dj.Lookup: {'fillcolor': 'azure3'}, dj.Imported: {'fillcolor': 'cornflowerblue'},
                  dj.Part: {'fillcolor': 'azure3', 'fontsize': '8'}}
    #
    root_name = root_rel().full_table_name
    root_id = add_node(name_lookup(root_name), node_attrs[dj.erd._get_tier(root_name)])
    for node_name, _ in root_dependencies.in_edges(root_name):
        if dj.erd._get_tier(node_name) is dj.erd._AliasNode:  # renamed attribute
            node_name = list(root_dependencies.in_edges(node_name))[0][0]
        node_id = add_node(name_lookup(node_name), node_attrs[dj.erd._get_tier(node_name)])
        dot.edge(node_id, root_id)
    for _, node_name in root_dependencies.out_edges(root_name):
        if dj.erd._get_tier(node_name) is dj.erd._AliasNode:  # renamed attribute
            node_name = list(root_dependencies.out_edges(node_name))[0][1]
        node_id = add_node(name_lookup(node_name), node_attrs[dj.erd._get_tier(node_name)])
        dot.edge(root_id, node_id)
    #render ERD
    filename = uuid.uuid4()
    dot.render('{}/{}'.format(UPLOAD_FOLDER, filename))
    #restriction form

    if request.method == 'POST' and 'enter' in request.form:
        if subtable is not None:
            flash('Adding entry to an Subtable')
        if request.form['enter'] == 'Enter':
            return redirect(url_for('main.enter', schema=schema, table=table, subtable=subtable))

    #create a table form datajoint relation
    ###
    return render_template(
            'schema.html',
            table_json=json_table(
                root_rel,
                projected=True,
                enter_url=url_for('main.enter', schema=schema, table=table, subtable=subtable),
                edit_url=url_for('main.edit', schema=schema, table=table, subtable=subtable),
                delete_url=url_for('main.delete', schema=schema, table=table, subtable=subtable),
                ),
            filename='{}.svg'.format(filename)
            )

@ping
@main.route('/erd/', defaults={'schema':None}, methods=['GET', 'POST'])
@main.route('/erd/<schema>', methods=['GET', 'POST'])
@login_required
def erd(schema):
    #draw ERD
    if schema is None or schema == 'None':
        rankdir = 'LR'
    else:
        rankdir = 'TB'
    graph_attr = {'size': '8, 24', 'rankdir': rankdir, 'splines': 'ortho'}
    node_attr = {'style': 'filled', 'shape': 'note', 'align': 'left', 'ranksep': '0.1',
                 'fontsize': '12', 'fontfamily': 'opensans', 'height': '0.2',
                 'fontname': 'Sans-Serif'}
    dot = graphviz.Digraph(graph_attr=graph_attr, node_attr=node_attr, engine='dot',
                           format='svg')

    def add_node(name, node_attr={}):
        """ Add a node/table to the current graph (adding subgraphs if needed). """
        table_names = dict(zip(['schema', 'table', 'subtable'], name.split('.')))
        graph_attr = {'color': 'grey80', 'style': 'filled', 'label': table_names['schema']}
        with dot.subgraph(name='cluster_{}'.format(table_names['schema']), node_attr=node_attr,
                          graph_attr=graph_attr) as subgraph:
            subgraph.node(name, label=name, URL=url_for('main.relation', **table_names),
                          target='_top', **node_attr)
        return name

    def name_lookup(full_name):
        """ Look for a table's class name given its full name. """
        pretty_name = dj.base_relation.lookup_class_name(full_name, schemata.__dict__)
        return pretty_name or full_name

    dependencies = dj.conn().dependencies
    dependencies.load()

    node_attrs = {dj.Manual: {'fillcolor': 'green3'}, dj.Computed: {'fillcolor': 'coral1'},
                  dj.Lookup: {'fillcolor': 'azure3'}, dj.Imported: {'fillcolor': 'cornflowerblue'},
                  dj.Part: {'fillcolor': 'azure3', 'fontsize': '8'}}
    #
    if schema is not None:
        schema_name = schema
        schema = getattr(getattr(schemata, str(schema)), 'schema_'+str(schema))
    for root_name in dependencies.node.keys():
        try:
            int(root_name)
            continue
        except:
            pass
        if schema is not None:
            if schema_name not in root_name:
                continue
        root_id = add_node(name_lookup(root_name), node_attrs[dj.erd._get_tier(root_name)])
        for _, node_name in dependencies.out_edges(root_name):
            if dj.erd._get_tier(node_name) is dj.erd._AliasNode:  # renamed attribute
                node_name = list(dependencies.out_edges(node_name))[0][1]
            node_id = add_node(name_lookup(node_name), node_attrs[dj.erd._get_tier(node_name)])
            dot.edge(root_id, node_id)
    #render ERD
    filename = uuid.uuid4()
    dot.render(os.path.join(UPLOAD_FOLDER, str(filename)))
    #
    return render_template('erd.html', filename='{}.svg'.format(filename))


