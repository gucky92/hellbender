from flask import render_template, request, jsonify, flash
from pymysql.err import InterfaceError, IntegrityError, OperationalError
import datajoint as dj
import axolotl
import sys
import os
import traceback

from ..errors import HellBenderError
from . import main

@main.errorhandler(403)
def forbidden(e):
    flash(str(e), 'error')
    if not request.accept_mimetypes.accept_html and request.accept_mimetypes.accept_json:
        response = jsonify({'error': 'Forbidden'})
        response.status_code = 403
        return response
    return render_template('403.html'), 403


@main.errorhandler(404)
def page_not_found(e):
    flash(str(e), 'error')
    if not request.accept_mimetypes.accept_html and request.accept_mimetypes.accept_json:
        response = jsonify({'error': 'Not found'})
        response.status_code = 404
        return response
    return render_template('404.html'), 404


@main.errorhandler(500)
def internal_server_error(e):
    flash(str(e), 'error')
    traceback.print_tb(e.__traceback__)
    if not request.accept_mimetypes.accept_html and request.accept_mimetypes.accept_json:
        response = jsonify({'error': 'Internal server error'})
        response.status_code = 500
        return response
    return render_template('500.html'), 500


@main.errorhandler(HellBenderError)
def hellbender_error(e):
    flash(str(e), 'error')
    traceback.print_tb(e.__traceback__)
    return render_template('error_message.html', error_type='HellBender Error', message=''), 500

@main.errorhandler(axolotl.AxolotlError)
def axolotl_error(e):
    flash(str(e), 'error')
    traceback.print_tb(e.__traceback__)
    return render_template('error_message.html', error_type='Axolotl Error', message=''), 500

@main.errorhandler(dj.DataJointError)
def datajoint_error(e):
    flash(str(e), 'error')
    traceback.print_tb(e.__traceback__)
    return render_template('error_message.html', error_type='DataJoint Error', message=''), 500

@main.errorhandler(InterfaceError)
def interface_error(e):
    flash(str(e), 'error')
    traceback.print_tb(e.__traceback__)
    if int(e.args[0]) in [0]:
        os.execv(sys.executable, ['python'] + sys.argv)
    return render_template('error_message.html', error_type='SQL Interface Error',
            message='An interface error may occur when a connection to the database cannot be established.'
            ' Contact the superuser immediatly, so that the website can be restarted.'), 500

@main.errorhandler(IntegrityError)
def integrity_error(e):
    flash(str(e), 'error')
    traceback.print_tb(e.__traceback__)
    return render_template('error_message.html', error_type='SQL Integrity Error',
            message='An integrity error may occur when you are trying to insert a value '
            'into the child table that does not exist in its parent table.'), 500

@main.errorhandler(OperationalError)
def operational_error(e):
    flash(str(e), 'error')
    traceback.print_tb(e.__traceback__)
    if int(e.args[0]) in [2006, 2013]:
        os.execv(sys.executable, ['python'] + sys.argv)
    return render_template('error_message.html', error_type='SQL Operational Error',
            message='An operational error may occur when a connection to the database cannot be established.'
            ' Contact the superuser immediatly, so that the website can be restarted.'), 500

@main.errorhandler(NotImplementedError)
def notimplemented_error(e):
    flash(str(e), 'error')
    traceback.print_tb(e.__traceback__)
    return render_template('error_message.html', error_type='Not Implemented Error',
            message='This option has not been implemented, if necessary immediatly ask superuser for advice.'), 500

#@main.errorhandler(Exception)
#def exception_error(e):
#    flash(str(e), 'error')
#    return render_template('500.html'), 500
