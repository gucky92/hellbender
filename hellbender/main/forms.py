from flask_wtf import FlaskForm as Form
from wtforms.validators import ValidationError, InputRequired, optional, EqualTo, Length
from wtforms import BooleanField, SelectField, StringField, \
        FormField, PasswordField, FieldList
import string_utils
import axolotl.schema as axolotl_schema
from importlib import reload
import os
from axolotl.create.from_dataframe import table_class_string

from .. import schemata
from ..schemata import SCHEMA
from ..schemata import CAN_ADD_COLUMNS
from ..dataform import DATATYPES, INTTYPES, STRINGTYPES, TIMETYPES, ENUMTYPES
from ..errors import HellBenderError

def snake_case_validator(form, field):
    string = field.data
    if string_utils.is_snake_case(string):
        return None
    elif string.islower() and string.isalpha():
        return None
    else:
        raise ValidationError(f'string is not snake case {string}')

def default_validator(form, field):
    default = field.data
    dtype = getattr(form, 'dtype').data
    if default == '' or default is None:
        pass
    elif dtype in TIMETYPES:
        if default == 'CURRENT_TIMESTAMP':
            pass
        else:
            raise ValidationError('Can only use CURRENT_TIMESTAMP as default')
    elif dtype in STRINGTYPES or dtype in ENUMTYPES:
        default = default.replace('"', '')
        if dtype in ENUMTYPES:
            dtype_option = getattr(form, 'dtype_option').data
            if not default in dtype_option:
                raise ValidationError('For enum default must be in dtype_option')
            dtype_option = '[' + dtype_option.replace('(', '') + ']'
            try:
                dtype_list = eval(dtype_option)
                if not all([isinstance(d, str) for d in dtype_list]) or not default in dtype_list:
                    raise ValidationError('list items must be strings and default must be in list')
            except:
                raise ValidationError('enumerate must be a list of string elements')

    elif dtype in INTTYPES:
        try:
            int(default)
        except:
            raise ValidationError(f'default for {dtype} must be int')
    else:
        raise ValidationError('cannot provide default')

def primary_key_check(form, field):
    if field.data:
        if getattr(form, 'nullable').data:
            raise ValidationError('cannot have a nullable primary key')

def dtype_validator(form, field):
    dtype = field.data
    if getattr(form, 'primary_key').data:
        if dtype in ['enum', 'evalenum', 'external', 'float', 'double', 'jsonstring', 'liststring']:
            raise ValidationError(f'cannot use dtype {dtype} as primary key')

def dtype_option_validator(form, field):
    dtype = getattr(form, 'dtype').data
    dtype_option = field.data
    if dtype in INTTYPES:
        if dtype_option == 'unsigned':
            pass
        elif dtype_option == 'autoincrement':
            pass
        elif dtype_option == '' or dtype_option is None:
            pass
        else:
            raise ValidationError('int type can only have unsigned or autoincrement as a dtype_option')
    elif dtype in STRINGTYPES:
        try:
            int(dtype_option)
        except ValueError:
            raise ValidationError('dtype_option for string type must be a integer')
    elif dtype in ENUMTYPES:
        dtype_option = '[' + dtype_option.replace('(', '') + ']'
        try:
            dtype_list = eval(dtype_option)
            if not all([isinstance(d, str) for d in dtype_list]):
                raise ValidationError('list items must be strings')
        except:
            raise ValidationError('enumerate must be a list of string elements')
    elif dtype_option is None or dtype_option == '':
        pass
    else:
        raise ValidationError('cannot provide a dtype_option')

class SnakeCaseOrFkValidator:

    def __call__(self, form, field):
        string = field.data
        if string_utils.is_snake_case(string):
            pass
        elif string.islower() and string.isalpha():
            pass
        elif '->' in string:
            fk = string.split('->')[-1].strip().split('(')[0].strip()
            fk = fk.split('.')
            schema_own = form.schema
            if len(fk) == 1:
                schema_name = schema_own
            else:
                schema_name = fk[-1]
            try:
                schema = getattr(schemata, schema_name)
            except AttributeError:
                raise ValidationError(f'schema {schema_name} does not exist')
            if schema_own == schema_name and len(fk) > 1:
                raise ValidationError(f'do not reference own schema in foreign key')
            elif schema_own != schema_name:
                try:
                    own_schema = getattr(schemata, schema_own)
                except AttributeError:
                    raise ValidationError(f'own schema does not exist')
                own_context = getattr(own_schema, 'schema_{}'.format(schema_own)).context
                if not schema_name in own_context:
                    raise ValidationError(f'schema not in context of own schema')
            ###
            table = fk[0]
            try:
                table = getattr(schema, table)
            except AttributeError:
                raise ValidationError(f'foreign table {table} does not exist')
        else:
            raise ValidationError(f'string is not snake case {string}')

def camel_case_validator(form, field):
    string = field.data
    if string_utils.is_camel_case(string):
        return None
    else:
        raise ValidationError(f'string is not camel case {string}')

def is_schema_validator(form, field):
    schema = field.data
    try:
        getattr(schemata, schema)
    except:
        raise ValidationError(f'schema {schema} does not exist')


class TableValidator:
    def __init__(self, exists):
        self.exists = exists

    def __call__(self, form, field):
        if self.exists:
            return self.is_table(form, field)
        else:
            return self.not_table(form, field)

    def not_table(self, form, field):
        table = field.data
        schema = getattr(form, 'schema').data
        try:
            schema = getattr(schemata, schema)
        except:
            raise ValidationError(f'schema {schema} does not exist')
        try:
            table = getattr(schema, table)
            raise ValidationError(f'table already exists {table.full_table_name}')
        except AttributeError:
            pass

    def is_table(self, form, field):
        table = field.data
        schema = getattr(form, 'schema').data
        try:
            schema = getattr(schemata, schema)
        except:
            raise ValidationError(f'schema {schema} does not exist')
        try:
            table = getattr(schema, table)
        except:
            raise ValidationError(f'table does not exit exist {table}')

def schema_validator(form, field):
    full_table_name = field.data
    if not '.' in full_table_name:
        raise ValidationError("table name must contain . to separate schema from table name")
    try:
        subtable = None
        schema, table = full_table_name.split('.')
    except:
        schema, table, subtable = full_table_name.split('.')
    try:
        schema = getattr(schemata, schema)
    except AttributeError:
        raise ValidationError(f'schema {schema} not in available schemata')
    try:
        table = getattr(schema, table)
    except AttributeError:
        raise ValidationError(f'table {table} not in schema {schema}')
    if subtable is not None:
        try:
            subtable = getattr(table, subtable)
        except AttributeError:
            raise ValidationError(f'subtable {subtable} not in table {table}')

class UserForm(Form):
    user = StringField('User', [InputRequired()])
    password = PasswordField('Password', [InputRequired()])

class RegisterForm(Form):
    user = StringField('User', [InputRequired()])
    password = PasswordField('Password', [InputRequired()])
    new_password = PasswordField('New Password', [InputRequired()])
    repeat_password = PasswordField('Repeat Password', [
        Length(min=10),
        InputRequired(),
        EqualTo('new_password', message='Passwords must match')])

class RestrictionForm(Form):
    restriction = StringField('Restriction')
    search = StringField('Search')
    update = StringField('Update')

class DeleteForm(Form):
    delete = BooleanField('Delete', default=False)


class AttributeForm(Form):
    choices = [(d, d) for d in DATATYPES] + [('foreign_key', 'foreign_key')]
    nullable = BooleanField('nullable', default=False)
    primary_key = BooleanField('primary_key', [primary_key_check], default=True)
    default = StringField('default', [optional(), default_validator])
    name = StringField('name', [InputRequired(), SnakeCaseOrFkValidator()])
    dtype = SelectField('dtype', [InputRequired(), dtype_validator], choices=choices)
    dtype_option = StringField('dtype_option', [dtype_option_validator, optional()])
    comment = StringField('comment', [optional()])

    @classmethod
    def add_attribute(cls, name, value):
        setattr(cls, name, value)

class SchemaForm(Form):
    schema_choices = [(s, s) for s in SCHEMA]
    schema = SelectField('schema', [InputRequired(), is_schema_validator], choices=schema_choices)
    table = StringField('table', [InputRequired(), camel_case_validator, TableValidator(True)])

class JoinForm(Form):
    table = FieldList(FormField(SchemaForm), min_entries=2)

def create_definition(attrs):
    """
    Parameters
    ----------
    attrs : iterable of AttributeForm validated.

    Returns
    -------
    definition : str
        definition for datajoint table class
    """
    #
    definition = "\n    "
    #
    primary_key = True
    for attr in attrs:
        ###
        d = attr.data
        if not d['primary_key'] and primary_key:
            primary_key = False
            definition += "---\n    "
        ###
        if '->' in d['name']:
            foreign_key = d['name']
            if d['nullable'] and not 'nullable' in foreign_key:
                foreign_key = foreign_key.replace('->', '-> [nullable]')
            definition += foreign_key
        else:
            attr_def = d['name']
            if d['nullable'] and not d['default']:
                attr_def += ' = null'
            elif d['default']:
                if d['dtype'] in ['enum', 'evalenum', 'char', 'varchar', 'liststring', 'jsonstring'] \
                    and not '"' in d['default']:
                    ###
                    attr_def += ' = "{}"'.format(d['default'])
                else:
                    attr_def += ' = {}'.format(d['default'])
            attr_def += ' : '
            if d['dtype'] in ['enum', 'evalenum', 'char', 'varchar']:
                d['dtype_option'] = d['dtype_option'].replace('(', '').replace(')', '')
                attr_def += '{}({})'.format(d['dtype'], d['dtype_option'])
            else:
                attr_def += d['dtype']
                if d['dtype_option'] is not None:
                    attr_def += ' ' + d['dtype_option']
            definition += attr_def
        if d['comment'] is not None:
            definition += ' #{}'.format(d['comment'])

        definition += "\n    "
        ###
    if primary_key:
        definition += "---\n    "
    return definition

def get_fields(fields):
    schema_name = fields.get('schema').data
    table_name = fields.get('table').data
    attrs = fields.get('attribute')
    #
    return schema_name, table_name, attrs

def return_schemafile(schema_name):
    schema_folder = os.path.split(axolotl_schema.__file__)[0]
    return os.path.join(schema_folder, '{}.py'.format(schema_name))

class AddColumnForm(Form):
    choices = [(o,o) for o in CAN_ADD_COLUMNS]
    schema = SelectField('schema', [InputRequired(), is_schema_validator], choices=choices)
    table = StringField('table', [InputRequired(), camel_case_validator, TableValidator(True)])
    attribute = FieldList(FormField(AttributeForm), min_entries=1)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for attr in self.attribute:
            attr.nullable.process_data(True)
            attr.primary_key.process_data(False)
            attr.add_attribute('schema', self.schema.data)

    def add_columns(self):
        schema_name, table_name, attrs = get_fields(self._fields)
        #
        definition = create_definition(attrs)
        #remove primary key distinction as cannot add primary key columns
        if not definition.startswith('\n    ---'):
            raise HellBenderError('Cannot add primary columns')
        definition = definition.replace('\n    ---', '#post-creation columns')
        #
        table = getattr(getattr(schemata, schema_name), table_name)
        table_definition = table.definition
        if '\n        ' in table_definition:
            definition = definition.replace('\n', '\n    ')
        new_definition = table_definition + definition
        #
        schema_file = return_schemafile(schema_name)
        #
        #
        f = open(schema_file, 'r')
        filedata = f.read()
        f.close()
        #replace the definition
        #
        lines = filedata.split('\n')
        for n, line in enumerate(lines):
            if 'class ' + table_name + '(' in line:
                class_line = n
                if not 'definition' in lines[n+1]:
                    raise HellBenderError('Table not formatted as expected in schema file, ask superuser')
                break
        else:
            raise HellBenderError('Could not find table in schema file, ask superuser')
        #
        table_class_definition = '\n'.join(lines[class_line:class_line+2])
        if not table_class_definition + table_definition in filedata:
            raise HellBenderError('Could not add columns due to schema file issues, ask superuser')
        #
        newfiledata = filedata.replace(
                table_class_definition+table_definition,
                table_class_definition+new_definition)
        #write to file
        f = open(schema_file, 'w')
        f.write(newfiledata)
        f.close()
        #
        try:
            reload(getattr(schemata, schema_name))
            getattr(getattr(schemata, schema_name), table_name)().add_columns()
        except Exception as e:
            f = open(schema_file, 'w')
            f.write(filedata)
            f.close()
            raise HellBenderError('Could not add column: ' + str(e))


class HeadingForm(Form):
    schema_choices = [(s, s) for s in SCHEMA]
    schema = SelectField('schema', [InputRequired(), is_schema_validator], choices=schema_choices)
    table = StringField('table', [InputRequired(), camel_case_validator, TableValidator(False)])
    attribute = FieldList(FormField(AttributeForm), min_entries=1)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for attr in self.attribute:
            attr.add_attribute('schema', self.schema.data)

    def insert_table_class(self):
        schema_name, table_name, attrs = get_fields(self._fields)
        #
        definition = create_definition(attrs)
        #
        schema_file = return_schemafile(schema_name)
        #
        #
        class_definition = table_class_string(
            schema_name, table_name, definition, 'Manual, dj.BaseMixin'
            )
        with open(schema_file, 'a') as f:
            f.write(class_definition)
        try:
            reload(getattr(schemata, schema_name))
        except Exception as e:
            lines = open(schema_file).readlines()
            open(schema_file).writelines(lines[:-len(class_definition.split('\n'))])
            ##
            raise HellBenderError('table could not be added: ' + str(e))
