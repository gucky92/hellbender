"""creating datatables from datajoint
"""

import flask_table
import pandas as pd
import datajoint as dj
from datajoint.utils import to_camel_case
from flask import url_for
from .errors import HellBenderError

class SelectCol(flask_table.Col):
    def td_format(self, content):
        html = '<select name="{}">'.format(content['name'])
        html += '<option></option>'
        for option, value in zip(content['options'], content.get('values',
                                                                 content['options'])):
            html += '<option value="{}"{}>{}</option>'.format(value, ' selected' if
            option == content.get('default', None) else '', option)
        html += "</select>"
        return html

class CheckBoxCol(flask_table.Col):
    def __init__(self, *args, checked=False, **kwargs):
        super().__init__(*args, **kwargs)
        self.checked = checked

    def td_format(self, content):
        html = '<input type="checkbox" name="{}" value="{}"{}>'.format(content['name'],
                                                                       content['value'],
                                                                       ' checked' if self.checked else '')
        return html

class SimpleCheckBoxCol(flask_table.Col):
    def __init__(self, *args, checked=False, **kwargs):
        super().__init__(*args, **kwargs)
        self.checked = checked

    def td_format(self, content):
        html = '<input type="checkbox"{}>'.format(' checked' if self.checked else '')
        return html

class CheckMarkCol(flask_table.Col):
    def td_format(self, content):
        return '<span class ="glyphicon {}" ></span>'.format('glyphicon-ok' if content
                                                              else 'glyphicon-remove')


class SimpleCheckMarkCol(flask_table.Col):
    def td_format(self, content):
        return '{}'.format('yes' if content else '')

class KeyColumn(flask_table.Col):
    def td_format(self, content):
        key = {name: content[name][0] for name in content.dtype.names}  # recarray to dict
        return '<code>{}</code>'.format(key)

class JobTable(flask_table.Table):
    classes = ['Relation']
    table_name = flask_table.Col('Table Name')
    status = flask_table.Col('Status')
    key = KeyColumn('Key')
    user = flask_table.Col('User')
    key_hash = flask_table.Col('Key Hash')
    error_message = flask_table.Col('Error Message')
    timestamp = flask_table.DatetimeCol('Timestamp')

    delete = CheckBoxCol('Delete')

class InfoTable(flask_table.Table):
    classes = ['table']
    attribute = flask_table.Col('Attribute')
    value = flask_table.Col('Value')

def list_digest(strings):
    #create a hash from a list of strings
    import hashlib, struct
    hash = hashlib.sha1()
    for s in strings:
        s = s.encode('utf-8')
        hash.update(struct.pack("I", len(s)))
        hash.update(s)
    return hash.hexdigest()



def json_table(
        rels, columns=None,
        restrictions={},
        table_name='supertable',
        enter_url=None,
        delete_url=None,
        edit_url=None,
        projected=False,
        deepjoin=None,
        recording_url=None,
        ):
    if not isinstance(rels, (list, tuple)):
        rels = [rels]
    elif deepjoin is not None or projected:
        raise HellBenderError('deepjoin or projected requires only single relation')
    def test_instance(rel):
        try:
            return rel()
        except:
            return rel
    rels = [test_instance(rel) for rel in rels]
    #return selection
    headings = [rel.heading.names for rel in rels]
    if columns is None and deepjoin is not None:
        raise HellBenderError('deepjoin requires columns specification')
    elif columns is not None and deepjoin is not None:
        selection = columns
    elif columns is not None and projected:
        raise HellBenderError('If projected, do not pass columns')
    elif columns is not None:
        selection = set(columns) & set().union(*headings)
    else:
        selection = set().union(*headings)
    #
    if deepjoin is not None:
        rel_proj = rels[0].deepjoin(columns=selection, restrictions=restrictions, **deepjoin)
    elif projected:
        #if projected is true selection and restrictions have already been applied
        rel_proj = rels[0]
    else:
        rel_proj = dj.superjoin(rels, columns=selection, restrictions=restrictions)

    table_json = {}
    table_json['enter_url'] = str(enter_url)
    table_json['delete_url'] = str(delete_url)
    table_json['recording_url'] = str(recording_url)
    table_json['edit_url'] = str(edit_url)
    table_json['id'] = table_name
    table_json['head'] = list(rel_proj.heading)
    data = pd.DataFrame(rel_proj.proj(*rel_proj.heading.non_blobs).fetch()).fillna('')
    for blob in rel_proj.heading.blobs:
        data[blob] = '<BLOB>'
    table_json['data'] = data[list(rel_proj.heading)].values
    return table_json

class DataJointTableFactory:
    """class to create and story datajoint tables
    """

    def __init__(self):
        self.store = {}

    def check_table(self,
            rels, url_id, columns=None,
            restrictions={},
            delete_column=False,
            delete_extra_kwargs={},
            edit_column=False,
            edit_url=None,
            edit_extra_kwargs={},
            projected=False,
            deepjoin=None,
            check_funcs=None,
            buttoncol=None,
            **url_kwargs):
        """
        Add table class to table store if not exists.

        Parameters
        ----------
        rels : list or dj.BaseRelation
            List of relations to join
        url_id : str
            The url ID for redirecting upon resorting table
        columns : list
            The columns to project. Required for deepjoin
        delete_column : dj.BaseRelation
            Whether to add a delete column to the table. If not BaseRelation
            only one dj.BaseRelation should exist, and it cannot be a Part Table.
        edit_column : dj.BaseRelation or list of dj.BaseRelation
            Whether to add a edit column to the table #TODO
        projected : bool
            If True, the relation is already fully projected
        deepjoin : bool or dict
            If True, deepjoin the BaseRelation. The dictionary is passed to
            the deepjoin function.
        check_funcs : dict
            Dictionary with column names to be applied to and the functions. TODO for specific columns
        url_kwargs : dict
            Extra arguments to pass to the redirect resorting function.

        Returns
        -------
        hash : str
            The hash for the table in store
        """
        if not isinstance(rels, (list, tuple)):
            rels = [rels]
        elif deepjoin is not None or projected:
            raise HellBenderError('deepjoin or projected requires only single relation')
        def test_instance(rel):
            try:
                return rel()
            except:
                return rel
        rels = [test_instance(rel) for rel in rels]
        #return selection
        headings = [rel.heading.names for rel in rels]
        if columns is None and deepjoin is not None:
            raise HellBenderError('deepjoin requires columns specification')
        elif columns is not None and deepjoin is not None:
            selection = columns
        elif columns is not None and projected:
            raise HellBenderError('If projected, do not pass columns')
        elif columns is not None:
            selection = set(columns) & set().union(*headings)
        else:
            selection = set().union(*headings)
        #
        if deepjoin is not None:
            rel_proj = rels[0].deepjoin(columns=selection, restrictions=restrictions, **deepjoin)
        elif projected:
            #if projected is true selection and restrictions have already been applied
            rel_proj = rels[0]
        else:
            rel_proj = dj.superjoin(rels, columns=selection, restrictions=restrictions)
        #
        #check if exists
        to_hash = rel_proj.heading.names + [str(deepjoin), str(projected), str(delete_column), str(edit_column)]
        hash_id = list_digest(to_hash)
        if hash_id in self.store:
            self.store[hash_id].update_url(url_id, **url_kwargs)
            setattr(self.store[hash_id], '_rel_proj', rel_proj)
            setattr(self.store[hash_id], '_check_funcs', check_funcs)
            return self.store[hash_id]
        #
        class SortableTableBase(flask_table.Table):
            _url_kwargs = url_kwargs
            _url_id = url_id
            _rel_proj = rel_proj
            _check_funcs = check_funcs

            def __init__(
                    self,
                    classes=None,
                    thead_classes=None,
                    sort_by=None,
                    sort_reverse=False,
                    no_items=None,
                    table_id=None,
                    border=None,
                    html_attrs=None,
                    **fetch_kwargs):
                rel_proj = self._rel_proj
                check_funcs = self._check_funcs
                data = pd.DataFrame(rel_proj.proj(*rel_proj.heading.non_blobs).fetch(**fetch_kwargs))
                #check_funcs checks if each entry returns true or not
                if check_funcs is not None:
                    for col, func in check_funcs.items():
                        data[col] = data.apply(lambda x: func(x.to_dict()), axis=1)
                for col in rel_proj.heading.blobs:
                    data[col] = '<BLOB>'
                #
                super().__init__(data.to_dict('records'), classes=classes, thead_classes=thead_classes,
                        sort_by=sort_by, sort_reverse=sort_reverse, no_items=no_items, table_id=table_id,
                        border=border, html_attrs=html_attrs)

            @classmethod
            def update_url(cls, url_id=None, **url_kwargs):
                setattr(cls, '_url_kwargs', url_kwargs)
                if url_id is not None:
                    setattr(cls, '_url_id', url_id)
                return cls

            def sort_url(self, col_key, reverse=False):
                if reverse:
                    direction = 'desc'
                else:
                    direction = 'asc'
                return url_for(self._url_id, sort=col_key, direction=direction, **self._url_kwargs)
        #
        #
        table_class = flask_table.create_table(
                hash_id,
                base=SortableTableBase,
                options=dict(
                    allow_sort=True
                    )
                )

        if edit_column is not False:
            if edit_column is not True and not isinstance(edit_column, (list, tuple)):
                rel = edit_column
                try:
                    rel = rel()
                except:
                    pass
            elif isinstance(edit_column, (list, tuple)):
                raise NotImplementedError('list for edit column')
            elif len(rels) != 1 and edit_column is True:
                raise HellBenderError("delete column only works with one relation")
            else:
                rel = rels[0]
            #
            if edit_url is None and not isinstance(rel, (dj.Manual, dj.Computed, dj.Imported)):
                raise HellBenderError(f"relation must be manual, computed, or imported to have deleted column: {type(rel)}")
            #
            url_kwargs = dict(**{key:key for key in rel.heading if key in rel_proj.heading})
            if edit_url is None:
                full_table_name = rel.full_table_name.replace('`', '').split('.')
                schema_name = full_table_name[0]
                #if isinstance(rel, dj.Part):
                #    table_split = full_table_name[1].split('__')
                #    table_name = to_camel_case(table_split[-2])
                #    subtable_name = to_camel_case(table_split[-1])
                #else:
                table_name = to_camel_case(full_table_name[1])

                url_kwargs_extra = {'schema':schema_name, 'table':table_name}
                if edit_extra_kwargs:
                    url_kwargs_extra.update(edit_extra_kwargs)
                edit_url = 'main.edit'
            else:
                url_kwargs_extra = edit_extra_kwargs
            table_class.add_column('EDIT', flask_table.ButtonCol(
                'EDIT', edit_url, url_kwargs=url_kwargs,
                url_kwargs_extra=url_kwargs_extra, allow_sort=False
            ))

        if delete_column is not False:
            if delete_column is not True:
                rel = delete_column
                try:
                    rel = rel()
                except:
                    pass
            elif len(rels) != 1 and delete_column is True:
                raise HellBenderError("delete column only works with one relation")
            else:
                rel = rels[0]
            #
            if isinstance(rel, dj.Part):
                raise HellBenderError("cannot use delete column on part table")
            elif not isinstance(rel, (dj.Manual, dj.Computed, dj.Imported)):
                raise HellBenderError(f"relation must be manual, computed, or imported to have deleted column: {type(rel)}")
            #
            url_kwargs = dict(**{key:key for key in rel.heading.primary_key})
            full_table_name = rel.full_table_name.replace('`', '').split('.')
            schema_name = full_table_name[0]
            table_name = to_camel_case(full_table_name[1])
            url_kwargs_extra = {'schema':schema_name, 'table':table_name}
            if delete_extra_kwargs:
                url_kwargs_extra.update(delete_extra_kwargs)
            #
            table_class.add_column('DELETE', flask_table.ButtonCol(
                'DELETE', 'main.delete', url_kwargs=url_kwargs,
                url_kwargs_extra=url_kwargs_extra, allow_sort=False
            ))
        #
        if buttoncol is not None:
            if not isinstance(buttoncol, dict):
                raise HellBenderError('buttoncol must be dictionary')
            table_class.add_column(buttoncol['name'], flask_table.ButtonCol(
                **buttoncol))
        #
        for col in rel_proj.heading:
            table_class.add_column(col, flask_table.Col(col))
        #
        if check_funcs is not None:
            for col in check_funcs:
                table_class.add_column(col, SimpleCheckMarkCol(col, allow_sort=False))
        #
        table_class.classes = ['Relation']
        #
        self.store[hash_id] = table_class
        #
        return self.store[hash_id]

    def __call__(self, hash_id):
        return self.store[hash_id]

