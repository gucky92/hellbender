from collections import defaultdict, OrderedDict
import os
import datetime
import pandas as pd
from wtforms import BooleanField, SelectField, DateField, DateTimeField, StringField, \
        ValidationError, FloatField, IntegerField, FileField, FormField, TextAreaField, \
        FieldList
        #SubmitField
from flask_wtf import FlaskForm as Form
from wtforms import Form as NoCsrfForm
from wtforms.widgets import HiddenInput
from werkzeug.utils import secure_filename
import datajoint as dj
from wtforms.validators import optional, length
from wtforms.validators import InputRequired as required
import numpy as np
from datajoint.computedmixin import JSON_DEFINABLE

from .errors import HellBenderError

#extensions and folder for upload external entries
UPLOAD_EXTENSIONS = set(['csv', 'npy'])
UPLOAD_FOLDER = '/mnt/engram/tmp'
#
DATATYPES = [
        'enum', 'external', 'varchar', 'char',
        'float', 'double', 'int', 'smallint', 'tinyint',
        'timestamp', 'date',
        'evalenum', 'jsonstring', 'liststring'
        ]
INTTYPES = ['int', 'smallint', 'tinyint']
STRINGTYPES = ['varchar', 'char']
TIMETYPES = ['timestamp']
ENUMTYPES = ['enum', 'evalenum']
TEXTAREA_NAMES = ['comments', 'description', 'definition', 'comment']
FK_DROPDOWN_LIMIT = 80
PARENT_ADD_OPTION_LIMIT = 5
NUMERICTYPES = {
     'float': np.float64,
     'double': np.float64,
     'tinyint': np.int64,
     'smallint': np.int64,
     'mediumint': np.int64,
     'int': np.int64,
     'bigint': np.int64,
     }
###
class ForeignKeyFormField(FormField):
    pass

def not_equal_attr(form, field):
    attr = field.data['attribute']
    if not field.data['use']:
        return None
    for field2 in form:
        if field2.label == field.label:
            continue
        elif field2.data['attribute'] == attr and field2.data['use']:
            raise ValidationError(f"cannot use the same attribute for different columns")

def check_extension(form, field):
    if field.data == '':
        raise ValidationError('No data uploaded to field')
    filename = field.data.filename
    if not ('.' in filename and filename.rsplit('.',1)[-1] in UPLOAD_EXTENSIONS):
        raise ValidationError(f"Wrong extension in filename {filename}, must be one of these: {UPLOAD_EXTENSIONS}")

def date_validator(form, field):
    try:
        try:
            datetime.datetime.strptime(str(field.data), '%Y-%m-%d')
        except ValueError:
            try:
                datetime.datetime.strptime(str(field.data), '%Y-%m-%d %H:%M')
            except ValueError:
                datetime.datetime.strptime(str(field.data), '%Y-%m-%d %H:%M:%S')
    except ValueError:
        raise ValidationError(f"Incorrect data format, should be YYYY-MM-DD HH:MM - {field.data}")

def date_return(data):
    if np.all(pd.isnull(data)):
        return None
    elif data is np.datetime64('NaT'):
        return None
    elif data is pd.NaT:
        return None
    elif data == 'NaT':
        return None
    elif data == 'nan':
        return None
    try:
        try:
            return datetime.datetime.strptime(str(data), '%Y-%m-%d')
        except ValueError:
            try:
                return datetime.datetime.strptime(str(data), '%Y-%m-%d %H:%M')
            except ValueError:
                return datetime.datetime.strptime(str(data), '%Y-%m-%d %H:%M:%S')
    except:
        return data

class ParentValidator:

    def __init__(self, parentrel, parentname=None):
        self.parentname = parentname
        self.parentrel = parentrel

    def __call__(self, form, field):
        d = field.data
        if self.parentname is None:
            d_parent = None
        else:
            d_parent = getattr(form, self.parentname).data
        if d_parent == '':
            d_parent = None
        if d == '':
            d = None
        if d == 'add_to_parent' and d_parent is None:
            raise ValidationError("Must specify parent if add_to_parent selected")
        if d_parent is not None:
            if d_parent == d:
                pass
            elif d == 'add_to_parent':
                pass
            else:
                raise ValidationError("parent and child field must match!")
        elif d is not None:
            if len(self.parentrel & {field.id:d}) == 0:
                raise ValidationError(f"value {d} not in parent table")

def field_factory(rel, attr, parent_table=None, parent_comment=None, parent_options=None, no_csrf=False):
    """function to create a wtf field from datajoint attribute.
    """
    def field_helper(attr, kwargs):
        if attr.type.startswith('int') or attr.type.replace(' unsigned', '') in ['smallint', 'tinyint', 'mediumint']:
            return IntegerField(**kwargs)
        elif attr.type == 'double' or attr.type == 'float':
            return FloatField(**kwargs)
        elif attr.type.startswith('varchar'):
            ###special attributes are varchar!!! (e.g. jsonstring, liststring)
            ml = int(attr.type.split('(')[-1][:-1])
            kwargs['validators'].append(length(max=ml))
            if attr.name in TEXTAREA_NAMES:
                return TextAreaField(**kwargs)
            elif attr.is_liststring:
                return TextAreaField(**kwargs)
            elif attr.is_jsonstring:
                return TextAreaField(**kwargs)
            elif attr.is_loadstring:
                return TextAreaField(**kwargs)
            else:
                return StringField(**kwargs)
        elif attr.type == 'date':
            kwargs['validators'].append(date_validator)
            del(kwargs['default'])
            return DateField(format='%Y-%m-%d', default=datetime.date.today, **kwargs)
        elif attr.type.startswith('enum'):
            choices = [(e[1:-1],e[1:-1]) for e in attr.type[attr.type.find('(')+1:attr.type.rfind(')')].split(',')]
            return SelectField(choices=choices, **kwargs)
        elif attr.type.startswith('char'):
            l = int(attr.type.split('(')[-1][:-1])
            kwargs['validators'].append(length(min=l, max=l))
            return StringField(**kwargs)
        elif attr.type == 'timestamp':
            kwargs['validators'].append(date_validator)
            del(kwargs['default'])
            return DateTimeField(format='%Y-%m-%d %H:%M', default=datetime.datetime.today, **kwargs)
        elif 'external' in attr.type or attr.type == 'blob':
            #kwargs['validators'] = [required(), check_extension]
            kwargs['validators'].append(check_extension)
            return FileField(**kwargs) #TODO add upload button
        else:
            raise NotImplementedError('FieldFactory does not know what to do with %s' % (attr.type))
    #
    #TODO primary keys of parent tables to display
    kwargs = defaultdict(list)
    kwargs['id'] = attr.name
    kwargs['label'] = (
            attr.comment if
            (not attr.comment is None)
            and (not 'TODO' in str(attr.comment))
            else '')
    #set default if exists
    if attr.nullable:
        kwargs['default'] = None
    elif attr.default == '""' or attr.default == '':
        #some error in datajoint
        kwargs['default'] = None
    else:
        kwargs['default'] = attr.default
    #
    if attr.in_key or (not attr.nullable and (attr.default == '""' or attr.default == '')):
        kwargs['validators'].append(required())
    elif (attr.nullable) or (attr.default is not None):
        kwargs['validators'].append(optional())
    ###
    if parent_table is not None:
        #
        if True:
        #if no_csrf:
            class ForeignKeyForm(NoCsrfForm):
                @classmethod
                def append_field(cls, name, field):
                    setattr(cls, name, field)
                    return cls
        else:
            class ForeignKeyForm(Form):
                @classmethod
                def append_field(cls, name, field):
                    setattr(cls, name, field)
                    return cls
        #add to label foreign key information
        kwargs['label'] += f' - references parent table {parent_comment}'
        label = kwargs['label']
        del(kwargs['label'])
        #
        add_dict = defaultdict(list)
        add_dict['id'] = 'add_to_parent'
        field = field_helper(attr, add_dict)
        #
        if parent_options is not None:
            if (attr.nullable) or (attr.default is not None):
                kwargs['default'] = ''
            if len(parent_table.heading) < PARENT_ADD_OPTION_LIMIT and not isinstance(parent_table, dj.Subquery):
                kwargs['validators'].append(ParentValidator(parentrel=parent_table, parentname='add_to_parent'))
                complete_field = SelectField(choices=parent_options, **kwargs)
                ForeignKeyForm.append_field(attr.name, complete_field)
                ForeignKeyForm.append_field('add_to_parent', field)
                return ForeignKeyFormField(ForeignKeyForm, label=label), True
            else:
                kwargs['label'] = label
                complete_field = SelectField(choices=parent_options, **kwargs)
                return complete_field, False
        else:
            if len(parent_table.heading) < PARENT_ADD_OPTION_LIMIT and not isinstance(parent_table, dj.Subquery):
                kwargs['validators'].append(ParentValidator(parentrel=parent_table, parentname='add_to_parent'))
                complete_field = field_helper(attr, kwargs)
                ForeignKeyForm.append_field(attr.name, complete_field)
                ForeignKeyForm.append_field('add_to_parent', field)
                return ForeignKeyFormField(ForeignKeyForm, label=label), True
            else:
                kwargs['label'] = label
                complete_field = field_helper(attr, kwargs)
                return complete_field, False

    return field_helper(attr, kwargs), False

def foreign_key_formatting(rel):
    """Given a dj.Relation return a dictionary of the attributes,
    which are foreign keys. Each key is an attribute name containing
    a dictionary with parent_table, data_options, and comment
    """
    attr_parent = {}
    for parent_table in rel.parent_tables():
        try:
            parent_table = parent_table()
        except:
            pass
        common_attrs = set(parent_table.heading.primary_key) & set(rel.heading)

        #fetch primary parent data
        parent_data = parent_table.proj().fetch()

        #check if parent_table is subquery
        if isinstance(parent_table, dj.Subquery):
            comment = '--RENAMED ATTRIBUTE--'
        else:
            comment = parent_table.full_table_name
        #
        parent_attr_no = len(parent_table.heading)
        #
        for attr in common_attrs:
            attr_data = np.unique(parent_data[attr])
            if len(attr_data) > FK_DROPDOWN_LIMIT:
                data_options = None
            else:
                data_options = [(d, d) for d in attr_data]
                if rel.heading[attr].nullable:
                    data_options.append(('', 'NULL'))
                if parent_attr_no < PARENT_ADD_OPTION_LIMIT:
                    data_options.append(('add_to_parent', '[add to parent]'))
            attr_parent[attr] = dict(
                    parent_table=parent_table,
                    parent_options=data_options,
                    parent_comment=comment
                    )
    return attr_parent

class UploadFormFactory:

    def __init__(self):
        self.store = {}

    def check_form(self, rel, data):
        """
        Parameters
        ----------
        rel : datajoint.Manual
        data : pandas.DataFrame
        """
        if not isinstance(data, pd.DataFrame):
            raise HellBenderError('data must be dataframe')
        try:
            rel = rel()
        except:
            pass
        if not isinstance(rel, dj.Manual):
            raise HellBenderError('relation must be Manual')
        #
        store_name = rel.full_table_name + str(data.columns)
        if store_name in self.store:
            return self.store[store_name]
        #
        class UploadForm(Form):
            option_fields = ['skip_duplicates']
            datetime_option = ['dayfirst', 'yearfirst']
            skip_fields = ['csrf_token']

            @classmethod
            def append_field(cls, name, field):
                setattr(cls, name, field)
                return cls

            @classmethod
            def create_uploadform(cls, rel, data):
                cls._rel = rel
                cls._data = data
                options = rel.heading
                choices = [(o, o) for o in options]
                cls.required = OrderedDict()
                class ColumnForm(Form):
                    use = BooleanField('use', default=False)
                    #TODO check attribues
                    attribute = SelectField('attribute', [required()], choices=choices)
                for column in data.columns:
                    cls.append_field(column, FormField(ColumnForm))
                    cls.required[column] = True
                cls.append_field('skip_duplicates', BooleanField('skip_duplicates', default=False))
                cls.append_field('dayfirst', BooleanField('dayfirst', default=False))
                cls.append_field('yearfirst', BooleanField('yearfirst', default=False))
                return cls

            def insert(self):
                rel = self._rel
                data = self._data
                ###
                for field in self:
                    if field.id in self.option_fields+self.datetime_option+self.skip_fields:
                        continue
                    if not field.id in data.columns:
                        raise HellBenderError(f'field {field.id} not in data {data.columns}, fix bug.')
                    if field.data['use']:
                        data = data.rename(columns={field.id:field.data['attribute']})
                    else:
                        data = data.drop(field.id, axis=1)
                if set(rel.heading.required_fields) - set(data.columns):
                    raise HellBenderError('required columns undefined')
                ###
                for name, attr in rel.heading.attributes.items():
                    if not name in data.columns:
                        continue
                    if attr.type in NUMERICTYPES:
                        data[name] = data[name].astype(NUMERICTYPES[attr.type])
                    elif attr.type in ['date']:
                        data[name] = data[name].apply(lambda x: date_return(x))
                    elif attr.type in ['timestamp']:
                        dayfirst = self.dayfirst.data
                        yearfirst = self.yearfirst.data
                        data[name] = data[name].apply(lambda x: pd.to_datetime(
                            x, dayfirst=dayfirst, yearfirst=yearfirst
                            ).to_pydatetime())
                    elif attr.type != 'external':
                        data[name] = data[name].astype(str)
                    else:
                        raise HellBenderError('Cannot insert type {attr.type}')
                #
                kwargs = {}
                for option in self.option_fields:
                    kwargs[option] = getattr(self, option).data
                try:
                    rel.insert(data.to_dict('records'), **kwargs)
                except dj.DataJointError as e:
                    raise HellBenderError(str(e))

        #
        ###
        return UploadForm.create_uploadform(rel, data)



class DataJointFormFactory:
    """class to create Forms from datajoint relations
    """

    def __init__(self):
        self.store = {}

    def check_form(self, rels, store_name, skip_parts=False, select_parts=None, parts_fields=False, multi_parts=False):
        """Check if form exists and create it if not
        """
        store_name += str(skip_parts) + str(select_parts) + str(parts_fields) + str(multi_parts)
        if not isinstance(rels, (list, tuple)):
            rels = [rels]
        if store_name in self.store:
            return self.store[store_name]
        #
        #
        def test_instance(rel):
            try:
                return rel()
            except:
                return rel
        rels = [test_instance(rel) for rel in rels]
        #
        if len(rels) == 1 and rels[0].table_name.endswith('approach'):
            class djForm(ApproachTableForm):
                pass
        else:
            class djForm(DataJointForm):
                pass

        self.store[store_name] = djForm.create_djform(rels, skip_parts, select_parts, parts_fields, multi_parts=multi_parts)
        return self.store[store_name]


    def __call__(self, name):
        return self.store[name]

class DataJointFormMeta:
    def process_input(self, kwargs):
        for k, v in kwargs.items():
            if k in self.required:
                kwargs[k] = date_return(v)
        return kwargs

    def update_foreign_key_select_fields(self):
        for k, attr_parent in self._foreign_keys.items():
            field = getattr(self, k)
            if isinstance(field, FormField):
                field = getattr(field, k)
            if isinstance(field, SelectField):
                parent_table = attr_parent['parent_table']
                entries = np.unique(parent_table.proj().fetch()[k]).tolist()
                null = [('', 'NULL')] if '' in dict(field.choices) else []
                add_to_parent = [('add_to_parent', '[add_to_parent]')] if 'add_to_parent' in dict(field.choices) else []
                field.choices = [(str(o), str(o)) for o in entries] + null + add_to_parent

    def process_foreign_input(self, kwargs):
        for k, v in kwargs.items():
            if self._parent_fields.get(k, False):
                field = getattr(self, k)
                field = getattr(field.args[0], k)
                field.kwargs['default'] = v
        return kwargs

    def process_part_input(self, kwargs):
        skip_parts = self._skip_parts
        select_parts = self._select_parts
        ###
        if skip_parts is True:
            return kwargs
        if not kwargs:
            return kwargs
        ###
        primary_kwargs = (set(self.required) & set(kwargs))
        if not primary_kwargs:
            return kwargs
        ###
        primary_kwargs = {k:v for k, v in kwargs.items() if k in primary_kwargs}
        ###
        for rel in self._rels:
            for part_table in rel.part_tables():
                if skip_parts is False and select_parts is None:
                    pass
                elif skip_parts is False:
                    if (part_table in select_parts) or (part_table.full_table_name in select_parts):
                        pass
                    else:
                        continue
                elif (part_table in skip_parts) or (part_table.full_table_name in skip_parts):
                    continue
                if not (set(part_table.heading) - set(primary_kwargs)):
                    continue
                r = (part_table & {k:v for k, v in primary_kwargs.items() if k in part_table.primary_key})
                if len(r) != 1:
                    continue
                else:
                    r = r.fetch1()
                    kwargs.update(r)
        return kwargs

    @classmethod
    def append_field(cls, name, field):
        setattr(cls, name, field)
        return cls

    def add_autoincrement(self, kwargs):
        for attr, rel in self._autoincrement.items():
            if attr in kwargs:
                continue
            try:
                max_value = np.max(rel.proj().fetch()[attr])
            except ValueError:
                max_value = 0
            kwargs[attr] = max_value + 1
        return kwargs
        #field = getattr(self, attr)
        #field.process_data(max_value + 1)

    def _get_data_helper(self, k, v, parent_tables):
        if k == 'REFERRER':
            return None
        elif v.data is None:
            return None
        elif v.data is np.nan:
            return None
        elif v.data == 'nan':
            return None
        elif v.data == '':
            return None
        #data type
        if self._parent_fields.get(k, False):
            parent_d = v.data['add_to_parent']
            if parent_d == '':
                parent_d = None
            elif parent_d is np.nan:
                parent_d = None
            d = v.data[k]
            if d == 'add_to_parent' and parent_d is None:
                return None
            elif (d == '') or (d is np.nan) or (d == 'nan'):
                d = None
            if parent_d is None and d is None:
                return None
            elif d == 'add_to_parent' or d == parent_d:
                d = parent_d
                parent_table = self._foreign_keys[k]['parent_table']
                parent_tables |= set([parent_table])
        elif isinstance(v, FileField):
            filename = secure_filename(v.data.filename)
            filepath = os.path.join(UPLOAD_FOLDER, filename)
            v.data.save(filepath)
            if filename.endswith('npy'):
                d = np.load(filepath)
            elif filename.endswith('csv'):
                d = pd.read_csv(filepath).to_records(False)
            else:
                raise HellBenderError('wrong file extension - BUG: must be checked previously!')
        else:
            d = v.data
        return d

    def insert(self, replace=False, skip_duplicates=False, update=False, fields=None, dat=None):
        """insert form data into axolotl database

        Parameters
        ----------
        replace : boolean
            whether to replace existing data
        skip_duplicates : boolean
            whether to skip duplicate entries
        update : boolean
            whether to update existing data (update has priority over replace)
        fields : OrderedDict of wtf.Fields
            Pass desired fields to insert directly (does not work with multi part tables).
        dat : dict
            Additional data to be inserted (will be overwritten if in fields of form).
        """
        rels = self._rels
        multi_parts = self._multi_parts
        skip_parts = self._skip_parts
        select_parts = self._select_parts
        if dat is None:
            dat = {}
        parent_tables = set()
        if fields is None:
            fields = self._fields
        #
        for k, v in fields.items():
            if isinstance(v, FieldList) and k in multi_parts:
                continue
            else:
                d = self._get_data_helper(k, v, parent_tables)
            if d is None:
                continue
            #
            dat[k] = d


        for parent_table in parent_tables:
            pdat = {k: v for k, v in dat.items() if k in parent_table.heading.primary_key}
            try:
                parent_table.insert1(pdat, skip_duplicates=True)
            except:
                for parent_table2 in parent_table.parent_tables(only_primary=True):
                    parent_table2.insert1(pdat, ignore_extra_fields=True, skip_duplicates=True)
                parent_table.insert1(pdat, skip_duplicates=True)

        for rel in rels:
            try:
                rel = rel()
            except:
                pass
            if update:
                restrictions = {k:v for k, v in dat.items() if k in rel.heading.primary_key}
                restricted_rel = (rel & restrictions)
                exists_dict = restricted_rel.fetch1()
                to_update = {k:v for k, v in dat.items()
                        if k in rel.heading.dependent_attributes
                        and np.all(v != exists_dict.get(k, None))
                        }
                restricted_rel.update(to_update)
                if not multi_parts:
                    for part_table in rel.part_tables():
                        if skip_parts is True:
                            continue
                        elif skip_parts is False and select_parts is None:
                            pass
                        elif skip_parts is False:
                            if (part_table in select_parts) or (part_table.full_table_name in select_parts):
                                pass
                            else:
                                continue
                        elif (part_table in skip_parts) or (part_table.full_table_name in skip_parts):
                            continue
                        #only update part table if primary key exists in part table
                        if len(part_table & restrictions) > 0:
                            exists_dict = (part_table & restrictions).fetch1()
                            to_update = {k:v for k, v in dat.items()
                                    if k in part_table.heading.dependent_attributes
                                    and np.all(v != exists_dict.get(k, None))
                                    }
                            (part_table & restrictions).update(to_update)
            else:
                rel.insert1(dat, ignore_extra_fields=True, replace=replace, skip_duplicates=skip_duplicates)
                if not multi_parts:
                    for part_table in rel.part_tables():
                        if skip_parts is True:
                            continue
                        elif skip_parts is False and select_parts is None:
                            pass
                        elif skip_parts is False:
                            if (part_table in select_parts) or (part_table.full_table_name in select_parts):
                                pass
                            else:
                                continue
                        elif (part_table in skip_parts) or (part_table.full_table_name in skip_parts):
                            continue
                        #
                        if set(part_table.heading.required_fields) - set(dat):
                            continue
                        part_table.insert1(
                                dat, ignore_extra_fields=True, replace=replace, skip_duplicates=skip_duplicates)

            for part_table_name in multi_parts:
                fieldlist = getattr(self, part_table_name)
                for idx, partform in enumerate(fieldlist):
                    partform.form.insert(replace=replace, skip_duplicates=skip_duplicates, update=update, dat=dat)


    @classmethod
    def create_djform(
            cls, rels, skip_parts=False,
            select_parts=None, parts_fields=False,
            multi_parts=False, skip_columns=[], no_csrf=False):
        """
        """
        def part_table_helper(part_table):
            if skip_parts is True:
                return False
            elif skip_parts is False and select_parts is None:
                return True
            elif skip_parts is False:
                if (part_table in select_parts) or (part_table.full_table_name in select_parts):
                    return True
                else:
                    return False
            elif (part_table in skip_parts) or (part_table.full_table_name in skip_parts):
                return False
            else:
                return True

        cls._rels = rels
        cls._skip_parts = skip_parts
        cls._select_parts = select_parts
        cls._parts_fields = parts_fields
        cls._multi_parts = []
        required = OrderedDict()
        autoincrement = {}
        foreign_keys = {}
        primary_keys = []
        _required = []
        parent_fields = {}
        assigned = skip_columns.copy()
        #
        for rel in rels:
            if isinstance(rel, dj.Part) and len(rels) == 1:
                pass
            elif not isinstance(rel, dj.Manual):
                raise HellBenderError(f"relation must be dj.Manual {type(rel)}")
            #
            attr_parent = foreign_key_formatting(rel)
            #
            for name, attr in rel.heading.attributes.items():
                if name not in assigned:
                    assigned.append(name)
                    if attr.in_key:
                        primary_keys.append(name)
                    if attr.nullable or attr.default is not None:
                        required[name] = False
                    else:
                        required[name] = True
                        _required.append(name)
                    if attr.autoincrement and attr.in_key:
                        autoincrement[name] = rel
                    parent_dict = attr_parent.get(name, {})
                    field, with_parent = field_factory(rel, attr, no_csrf=no_csrf, **parent_dict)
                    cls.append_field(
                            name, field
                            )
                    if parent_dict:
                        foreign_keys[name] = parent_dict
                    if with_parent:
                        parent_fields[name] = True
                    else:
                        parent_fields[name] = False
            for part_table in rel.part_tables():
                if not part_table_helper(part_table):
                    continue
                #
                try:
                    part_table = part_table()
                except:
                    pass
                #
                attr_parent = foreign_key_formatting(part_table)
                ###allows for multiple multi part tables
                if multi_parts:
                    class DataJointPartTableForm(DataJointFormNoCsrf):
                        pass

                    #check for other primary keys to ascertain if only one entry or more
                    #are allowed in the part table.
                    extra_primary_key = set(part_table.heading.primary_key) - set(rel.heading.primary_key)
                    if len(extra_primary_key) == 0:
                        max_entries = 2
                    else:
                        max_entries = None

                    fieldclass = DataJointPartTableForm.create_djform(
                            [part_table], skip_columns=assigned, no_csrf=True)

                    fieldlist = DataJointFieldList(FormField(fieldclass), min_entries=1, max_entries=max_entries)

                    cls.append_field(part_table.__class__.__name__, fieldlist)
                    cls._multi_parts.append(part_table.__class__.__name__)

                else:
                    for name, attr in part_table.heading.attributes.items():
                        if not name in assigned:
                            assigned.append(name)
                            if attr.in_key:
                                primary_keys.append(name)
                            if attr.nullable or attr.default is not None:
                                required[name] = False
                            else:
                                required[name] = True
                                _required.append(name)
                            parent_dict = attr_parent.get(name, {})
                            field, with_parent = field_factory(
                                    part_table, attr, no_csrf=no_csrf, **parent_dict)
                            cls.append_field(
                                    name, field
                                    )
                            if parent_dict:
                                foreign_keys[name] = parent_dict
                            if with_parent:
                                parent_fields[name] = True
                            else:
                                parent_fields[name] = False

        #
        cls.required = required
        cls._required = _required
        cls._primary_keys = primary_keys
        cls._autoincrement = autoincrement
        cls._foreign_keys = foreign_keys
        cls._parent_fields = parent_fields

        cls.append_field('REFERRER', StringField(label='REFERRER',widget=HiddenInput()))
        return cls

class DataJointForm(Form, DataJointFormMeta):
    def __init__(self, *args, **kwargs):
        kwargs = self.process_input(kwargs)
        kwargs = self.process_part_input(kwargs)
        kwargs = self.process_foreign_input(kwargs)
        kwargs = self.add_autoincrement(kwargs)
        super().__init__(*args, **kwargs)
        self.update_foreign_key_select_fields()

class DataJointFieldList(FieldList):
    pass

class DataJointFormNoCsrf(NoCsrfForm, DataJointFormMeta):
    #pass
    def __init__(self, *args, **kwargs):
        kwargs = self.process_input(kwargs)
        kwargs = self.process_part_input(kwargs)
        kwargs = self.process_foreign_input(kwargs)
        kwargs = self.add_autoincrement(kwargs)
        super().__init__(*args, **kwargs)
        self.update_foreign_key_select_fields()

class ApproachTableForm(DataJointForm):

    @classmethod
    def create_djform(cls, *args, **kwargs):

        cls = super().create_djform(*args, **kwargs)

        for key in JSON_DEFINABLE:
            cls.append_field(key, StringField(key, [optional()]))

        return cls

    def insert(self, replace=False, skip_duplicates=False, update=False):
        fields = {}
        json = {}
        for k, v in self._fields.items():
            if v.data in ['', None, 'nan', np.nan]:
                continue
            elif k == 'json':
                json.update(eval(v.data))
            elif k in JSON_DEFINABLE:
                try:
                    d = eval(v.data)
                except:
                    d = v.data
                #
                json[k] = d
            else:
                fields[k] = v
        #hacky way
        class Data:
            data = json
        #
        fields['json'] = Data

        super().insert(replace=replace, skip_duplicates=skip_duplicates, update=update, fields=fields)
