#!/usr/bin/env python

# Disable matplotlib display
import matplotlib
matplotlib.use('Agg')
del matplotlib
#
from flask import Flask
from flask_login import LoginManager, UserMixin
from flask_bootstrap import Bootstrap
import os
from .dataform import DataJointFormFactory
from .datatable import DataJointTableFactory
from .dataform import UploadFormFactory
from .schemata import experimenters
from werkzeug.security import generate_password_hash, check_password_hash
from flask import redirect, request
#
import axolotl
import datajoint as dj
from importlib import reload
#
CREATE_HOST_URL = 'https://129.236.163.137:2434/heading/'
UPLOAD_FOLDER = '/mnt/engram/tmp'
ALLOWED_EXTENSIONS = set(['csv'])
#
# Create flask application
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
login_manager = LoginManager()
login_manager.init_app(app)
upload_factory = UploadFormFactory()
form_factory = DataJointFormFactory()
table_factory = DataJointTableFactory()

class User(UserMixin):
    # proxy for a database of users
    #TODO move to init?
    try:
        user_database = dict([(p, p) for p in experimenters.Experimenter().fetch('experimenter_name')])
    except Exception as e:
        print(e)
        reload(dj)
        reload(axolotl)
        axolotl.conn()
        dj.conn()
        user_database = dict([(p, p) for p in experimenters.Experimenter().fetch('experimenter_name')])
    ###
    for user in experimenters.Experimenter().iterrows():
        if user.fetch1()['password_hash'] is None:
            user.update({'password_hash':generate_password_hash('savethefruitflies!')})

    def __init__(self, user):
        self.user = user
        self.id = user
        self.entry = (experimenters.Experimenter() & {'experimenter_name':self.user})

    def check_user(self):
        if not self.user in self.user_database:
            return None
        else:
            return self.user

    def set_password(self, password):
        self.entry.update({'password_hash': generate_password_hash(password)})

    def check_password(self, password):
        password_hash = self.entry.fetch1()['password_hash']
        is_active = eval(self.entry.fetch1()['is_active'])
        if not is_active:
            return False
        if password is None or password_hash is None:
            return False
        return check_password_hash(password_hash, password)

    @classmethod
    def get(cls,user_id):
        return cls.user_database.get(user_id)

@login_manager.user_loader
def load_user(user_id):
    try:
        return User(User.get(user_id))
    except:
        reload(dj)
        reload(axolotl)
        axolotl.conn()
        dj.conn()
    return User(User.get(user_id))

@login_manager.unauthorized_handler
def unauthorized_callback():
    return redirect('/login?next=' + request.path)

#app.config.from_object(DevelopmentConfig)
app.secret_key = b'\xfd\xca\x03@\xed\xa6\xe6\x05\xeb\xe5#\xdb\xa4\xa8\x9a\x16%D\xbd\tI`h/'
# Register extensions
bootstrap = Bootstrap(app)
from flask_sslify import SSLify
sslify = SSLify(app)

# Register blueprints
from .main import main as main_blueprint
#from .images import images as image_blueprint
app.register_blueprint(main_blueprint)
#app.register_blueprint(image_blueprint, )
###
