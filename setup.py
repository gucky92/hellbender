# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='hellbender',
    version='0.0.0',
    description='Database Website for the Behnia Lab',
    long_description=readme,
    author='Matthias Christenson',
    author_email='matchristenson@gmail.com',
    url='',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
